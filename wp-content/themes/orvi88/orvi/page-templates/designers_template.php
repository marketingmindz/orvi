<?php 
/*
** Template name: Designers Page
*/
get_header('blog');
?>
    <article class="content">
    	<div class="">
    	<div id="grid-gallery" class="grid-gallery">
    	<section class="grid-wrap">
    
    		<div class="grid" id="isotope-container">
				<?php 
                $z = 1;
                $x = 1;
                $args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
                $wpex_query = new WP_Query( $args );
                while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                
                if( $x%2 == 1 ){
                $class_home = 'small';
                }else{
                $class_home = 'big';
                }
                
                if($z%2==0) $x++;
                ?>
                	<figure class="single-item-effect <?php echo $class_home ?>">
						<?php
                        if ( has_post_thumbnail()):
                        the_post_thumbnail('catalogue-featured-image');
                        endif;
                        ?> 
                        <figcaption>
                        	<div class="figcaption-border">
                        	<h2><?php the_title(); ?></h2>
                        	<a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                        	<div class="figure-overlay"></div>
                        	</div>
                        </figcaption>												
                    </figure>
    			<?php $z++; endwhile;wp_reset_postdata(); ?>
    
    
    
    		</div>
    	</section><!-- // grid-wrap -->
    
    	</div><!-- // grid-gallery -->
    	</div>
    </article>
<?php get_footer(); ?>