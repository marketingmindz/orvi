<?php
/*
** Template name:Category Page
*/
get_header('blog');
?>  
          <?php if($_SESSION['_range'] == "x"){ ?>
            <article style="float:left; width:40%;">
            	
                <div>
            <div class="v-range-align"><img src="http://localhost/projects2/orvi/wp-content/themes/orvi/img/white-logo.png" alt="Orvi" width="120"></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/x-range.png" /></div>
            <div class="v-range-align"><p><?php echo get_option('wpc_xrange'); ?></p></div>
        </div>
                
                
                
            </article>
           	
			
            <article class="content no-padding color-x" style="float:right; width:60%;">
                <div class="no-margin">
                	<div class="grid">
						<?php
						$term_id = 46;
						$taxonomy_name = 'orvicat';
						$termchildren = get_term_children( $term_id, $taxonomy_name );
						?>
                        <?php 
                        foreach ( $termchildren as $child ) {
							
							if( $i % 3 == 0 ){
								$class_name = 'full';
							}else{
								$class_name = 'big';
							}
							$term = get_term_by( 'id', $child, $taxonomy_name );
							
						?>
                        
                        <figure class="single-item-effect <?php echo $class_name ?>">
                        	<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
							<img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                       
							<figcaption>
								<div class="figcaption-border">
                                
									<h2><?php echo $term->name; ?></h2>
									<a href="<?php echo get_term_link( $child, $taxonomy_name ); ?>"><?php _e('View more','orvi'); ?></a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
							
						
                        <?php $i++; } ?> 
                    </div>
                </div>
            </article>
            <?php } ?>
            
            <?php if($_SESSION['_range'] == "v"){ ?>
			<article style="float:left; width:40%;">
            	
                <div>
            <div class="v-range-align"><img src="http://localhost/projects2/orvi/wp-content/themes/orvi/img/logo.png" alt="Orvi" width="120"></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-range.png" /></div>
            <div class="v-range-align"><p><?php echo get_option('wpc_vrange'); ?></p></div>
        </div>
                
                
                
            </article>
            <article class="content no-padding color-v" style="float:right; width:60%;">
                <div class="no-margin">
                	<div class="grid">
					<?php
					$term_id = 47;
					$taxonomy_name = 'orvicat';
					$termchildren = get_term_children( $term_id, $taxonomy_name );
					?>
                     <?php 
                        foreach ( $termchildren as $child ) {
							
							if( $i % 3 == 0 ){
								$class_name = 'full';
							}else{
								$class_name = 'big';
							}
							$term = get_term_by( 'id', $child, $taxonomy_name );
							
						?>
                        
                        <figure class="single-item-effect <?php echo $class_name ?>">
                        	<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
							<img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                       
							<figcaption>
								<div class="figcaption-border">
                                
									<h2><?php echo $term->name; ?></h2>
									<a href="<?php echo get_term_link( $child, $taxonomy_name ); ?>"><?php _e('View more','orvi'); ?></a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
							
						
                        <?php $i++; } ?>    
                    </div>
                </div>
            </article>
            <?php } ?>
<?php get_footer(); ?>