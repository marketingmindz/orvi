<?php
/*
** Template name: Blog page
*/
get_header('blog');
?>
	<article class="content">
    	<div class="">
    	<div id="grid-gallery" class="grid-gallery">
    	<section class="grid-wrap">
    
    		<div class="container_blog">
				<?php 
                
                $args = array( 'post_type' => 'post', 'posts_per_page' =>-1,'order'=>'ASC');
                $wpex_query = new WP_Query( $args );
                while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                
                ?>
                	<div class="blogs_part">
						
                        
                        	<div class="blogs_part_inn">
                        	<h2><?php the_title(); ?></h2>
                            <h4><?php echo get_the_date(); ?> ,<span><?php echo $author = get_the_author(); ?></span></h4>
							<?php
							if ( has_post_thumbnail()):
							the_post_thumbnail('catalogue-featured-image');
							endif;
							?> 
                            <?php
								echo wp_trim_words( get_the_content(), 20, '...' );
							?>
                        	<a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                        	<div class="figure-overlay"></div>
                        	</div>
                        												
                    </div>
    			<?php  endwhile;wp_reset_postdata(); ?>
    		</div>
    	</section><!-- // grid-wrap -->
    
    	</div><!-- // grid-gallery -->
    	</div>
    </article>
<?php get_footer(); ?>