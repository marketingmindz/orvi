<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles.css" />
	<?php if($_SESSION['_range'] == "v"){ ?>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles_v.css" />
	<?php } ?>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize.css" /><!-- CSS: Normalize -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" /><!-- CSS: Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css" />
    <?php wp_head(); ?>
    
</head>



<body <?php body_class(); ?>>

<div id="container" class="container">
	
	<?php if(is_page('home')){ ?>
        <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0" style="display:none;">
        	<span class="cd-menu-icon"></span>
        </a>
        
        <div id="travelogue-search" class="travelogue-search" style="display:none;">     
			<?php get_search_form(); ?>
        </div>
    <?php } else { ?>
        <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
        <span class="cd-menu-icon"></span>
        </a>
    
        <div id="travelogue-search" class="travelogue-search">     
        	<?php get_search_form(); ?>
        </div>
    <?php } ?> 