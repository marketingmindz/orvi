<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */

get_header('blog'); ?>
	<div class="container_full_80">
    <div class="full_title">
<?php while ( have_posts() ) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
	</div>
    <div class="container_blog">
        <div class="blog_content_left">
            <?php
                if ( has_post_thumbnail()):
                the_post_thumbnail('catalogue-featured-image');
                endif;
            ?>
        </div>
        <div class="blog_content_right">
            <?php the_content(); ?>
        </div>
    </div>    
     <?php endwhile; ?>
</div><!-- .content-area -->
<?php get_template_part( 'prev-next-blog', get_post_format() ); ?>
<?php get_footer(); ?>
