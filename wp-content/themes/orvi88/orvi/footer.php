<?php 
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "container" div and all content after.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
?>
    
     </div><!-- /container -->


        
		<div id="st-container" class="st-container"></div>
		
        
		<?php wp_footer(); ?>
		
		<?php if(!is_page(404)){ ?>
		<script src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.js"></script>
        <?php } ?>
		
		
		
		<?php if(!is_single() && !is_page('blog')){  ?>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
		<?php } ?>
        
        <script src="<?php bloginfo('template_url'); ?>/js/classie.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/sidebarEffects.js"></script>


		<?php if(is_page('home') && is_page('media-center') && !is_page('blog')){ ?>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.infinitescroll.js"></script>
        <?php } ?>
       
		
        <script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
        
        
		<?php
		if(!is_front_page()){
			if(!is_page(404) && !is_page('about-us-x') && !is_page('about-us-v') && !is_page('where-to-buy') && !is_page('blog')){ ?>
			<script src="<?php bloginfo('template_url'); ?>/js/transition.js"></script>
			<?php }
		}
		?>
        
        
		
        
        <?php if(is_page(404)|| is_page('single')){ ?>
		<script src="<?php bloginfo('template_url'); ?>/js/modernizr.form.custom.js"></script>
		<?php
        }
		?>
        
        
       
		
        
		<?php /*if(is_page('contact-us')){ ?>
        <script src="<?php bloginfo('template_url'); ?>/js/stepsForm.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/contact.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.form.js"></script>
        <?php }*/ ?>
        
		
		
		<?php if(is_page('gallery')){ ?>
        <script src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.gallery.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/cbpGridGallery.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/isotope/modernizr-isotope.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/isotope/isotope.pkgd.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/isotope/imagesloaded.pkgd.min.js"></script>
        <?php } ?>
        		     
		
		
		<?php if(is_page('single')){ ?>
		<script src="<?php bloginfo('template_url'); ?>/js/stepsFormComment.js"></script>
        
        <?php } ?>
        
        
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.ketchup.all.min.js"></script>
        

<script type="text/javascript">
	jQuery( document ).ready(function( $ ) {
		new UISearch( document.getElementById( 'travelogue-search' ) );
		jQuery('#st-container').load('<?php bloginfo('template_url'); ?>/nav.php', function() {
			//Begin: MailChimp JS
			jQuery('#invite').ketchup().submit(function(evt) {
				evt.preventDefault();
				if (jQuery(this).ketchup('isValid')) {
					var action = jQuery(this).attr('action');
	
					jQuery.ajax({
						url: action,
						type: 'POST',
						data: {
							email: jQuery('#address').val()
						},
						success: function(data){
							jQuery('#result').html(data).css('color', '#35cf76');
						},
						error: function() {
							jQuery('#result').html('Sorry, an error occurred.').css('color', '#e74c3c');
						}
					});
				}else{
					jQuery('#result').html('Please enter an valid email address.').css('color', '#e74c3c');
				}
				return false;
			});
			//End: MailChimp JS
		});
		jQuery( "#trigger-menu" ).click(function() {
			jQuery('.travelogue-search').fadeToggle('fast');
		});
	});
	
</script>


	<?php if(is_page() && !is_front_page() && !is_page("gallery")  && !is_page("contact-us") && !is_single()
    && !is_page("orvicat") && !is_page('installation') && !is_page('media-center') && !is_page('terms-conditions')){ ?>
<script>
	(function() {

		// detect if IE : from http://stackoverflow.com/a/16657946		
		var ie = (function(){
			var undef,rv = -1; // Return value assumes failure.
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf('MSIE ');
			var trident = ua.indexOf('Trident/');

			if (msie > 0) {
				// IE 10 or older => return version number
				rv = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			} else if (trident > 0) {
				// IE 11 (or newer) => return version number
				var rvNum = ua.indexOf('rv:');
				rv = parseInt(ua.substring(rvNum + 3, ua.indexOf('.', rvNum)), 10);
			}

			return ((rv > -1) ? rv : undef);
		}());


		// disable/enable scroll (mousewheel and keys) from http://stackoverflow.com/a/4770179					
		// left: 37, up: 38, right: 39, down: 40,
		// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
		var keys = [32, 37, 38, 39, 40], wheelIter = 0;

		function preventDefault(e) {
			e = e || window.event;
			if (e.preventDefault)
			e.preventDefault();
			e.returnValue = false;  
		}

		function keydown(e) {
			for (var i = keys.length; i--;) {
				if (e.keyCode === keys[i]) {
					preventDefault(e);
					return;
				}
			}
		}

		function touchmove(e) {
			preventDefault(e);
		}

		function wheel(e) {
			// for IE 
			//if( ie ) {
				//preventDefault(e);
			//}
		}

		function disable_scroll() {
			window.onmousewheel = document.onmousewheel = wheel;
			document.onkeydown = keydown;
			document.body.ontouchmove = touchmove;
		}

		function enable_scroll() {
			window.onmousewheel = document.onmousewheel = document.onkeydown = document.body.ontouchmove = null;  
		}

		var docElem = window.document.documentElement,
			scrollVal,
			isRevealed, 
			noscroll, 
			isAnimating,
			container = document.getElementById( 'container' ),
			trigger = container.querySelector( 'button.trigger' );

		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}
		
		function scrollPage() {
			scrollVal = scrollY();
			
			if( noscroll && !ie ) {
				if( scrollVal < 0 ) return false;
				// keep it that way
				window.scrollTo( 0, 0 );
			}

			if( classie.has( container, 'notrans' ) ) {
				classie.remove( container, 'notrans' );
				return false;
			}

			if( isAnimating ) {
				return false;
			}
			
			if( scrollVal <= 0 && isRevealed ) {
				toggle(0);
			}
			else if( scrollVal > 0 && !isRevealed ){
				toggle(1);
			}
		}

		function toggle( reveal ) {
			isAnimating = true;
			
			if( reveal ) {
				classie.add( container, 'modify' );
			}
			else {
				noscroll = true;
				disable_scroll();
				classie.remove( container, 'modify' );
			}

			// simulating the end of the transition:
			setTimeout( function() {
				isRevealed = !isRevealed;
				isAnimating = false;
				if( reveal ) {
					noscroll = false;
					enable_scroll();
				}
			}, 600 );
		}

		// refreshing the page...
		var pageScroll = scrollY();
		noscroll = pageScroll === 0;
		
		disable_scroll();
		
		if( pageScroll ) {
			isRevealed = true;
			classie.add( container, 'notrans' );
			classie.add( container, 'modify' );
		}
		
		window.addEventListener( 'scroll', scrollPage );
		trigger.addEventListener( 'click', function() { toggle( 'reveal' ); } );
	})();
</script>


<?php if(is_front_page()){ ?>
<script>
	var index = 1;
	var t;

	$('.content > div > .grid').infinitescroll({
		debug		 	: false,
		loading: {
			finished	: function( opts ){},
			start		: startAjax,
		},				
		navSelector  	: "#infinitescroll",
		nextSelector 	: "#infinitescroll",
		itemSelector 	: "figure",
		dataType	 	: 'html',
		maxPage         : 4,
		path: function(index) {
			var count = parseInt(index)-1;
			return "ajax/content-"+count+".html";
		},
	});

	function startAjax( opts ) {
		$.infinitescroll.prototype.options = opts;
		$.infinitescroll.prototype.beginAjax(opts);
	}

</script>
<?php } ?>



<?php if(is_page("gallery")){ ?>
<script type="text/javascript">

	var navigationContainer = $('#filter-nav'),
	mainNavigation = navigationContainer.find('#filter-main-nav ul');

	$('.filter-nav-trigger').on('click', function(event){
		event.preventDefault();
		$(this).toggleClass('menu-is-open');
		mainNavigation.off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend').toggleClass('is-visible');
	});

	</script>
	<script>
		new CBPGridGallery( document.getElementById( 'grid-gallery' ),{isotope:true} );
</script>
<?php } ?>

<?php } ?>

</body>
</html>