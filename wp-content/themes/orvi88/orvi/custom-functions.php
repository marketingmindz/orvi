<?php
/*** Products Post-type ***/

add_action('init', 'create_orviproducts');

function create_orviproducts() {

    $labels_pt = array(
        'name' => __('Orvi Products', 'post type general name','orvi'),
        'singular_name' => __('Orvi Products', 'post type singular name','orvi'),
        'add_new' => __('Add Orvi Products','orvi'),
        'add_new_item' => __('Add Orvi Products','orvi'),
        'edit_item' => __('Edit Orvi Products','orvi'),
        'new_item' => __('New Orvi Products','orvi'),
        'view_item' => __('View Orvi Products','orvi'),
        'search_items' => __('Search Orvi Products','orvi'),
        'not_found' =>  __('Nothing found','orvi'),
        'not_found_in_trash' => __('Nothing found in Trash','orvi'),
        'parent_item_colon' => ''
    );

    $args_pt = array(
        'labels' => $labels_pt,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
      ); 

    register_post_type( 'orvi_products' , $args_pt );



	$labels_tx = array(
		'name'              => __( 'Product Categories'),
		'singular_name'     => __( 'Product Category'),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Product Categories' ),
	);

	$args_tx = array(
		'hierarchical'      => true,
		'labels'            => $labels_tx,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array('slug' =>  'orvicat'),
	);

	register_taxonomy('orvicat', array('orvi_products'), $args_tx );
	
	$labels_ct = array(
		'name'              => __( 'Countries'),
		'singular_name'     => __( 'Countries'),
		'search_items'      => __( 'Search Countries' ),
		'all_items'         => __( 'All Countries' ),
		'parent_item'       => __( 'Parent Countries' ),
		'parent_item_colon' => __( 'Parent Countries:' ),
		'edit_item'         => __( 'Edit Countries' ),
		'update_item'       => __( 'Update Countries' ),
		'add_new_item'      => __( 'Add New Countries' ),
		'new_item_name'     => __( 'New Countries Name' ),
		'menu_name'         => __( 'Countries' ),
	);

	$args_ct = array(
		'hierarchical'      => true,
		'labels'            => $labels_ct,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array('slug' =>  'countries'),
	);

	register_taxonomy('countries', array('orvi_products'), $args_ct );
	
}

/*** Enquiries Post-type ***/

add_action('init', 'create_enquiries');

function create_enquiries() {

    $labels_pt = array(
        'name' => __('Enquiries', 'post type general name','orvi'),
        'singular_name' => __('Enquiries', 'post type singular name','orvi'),
        'view_item' => __('View Enquiries','orvi'),
        'search_items' => __('Search Enquiries','orvi'),
        'not_found' =>  __('Nothing found','orvi'),
        'not_found_in_trash' => __('Nothing found in Trash','orvi'),
        'parent_item_colon' => ''
    );

    $args_pt = array(
        'labels' => $labels_pt,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'title'),
		'show_in_menu' => 'edit.php?post_type=orvi_products'
      ); 

    register_post_type( 'enquiries' , $args_pt );
}




///////////////Designers Post Type//////////////

add_action('init', 'create_designers');

function create_designers() {

    $labels = array(
        'name' => __('Orvi Designers', 'post type general name','orvi'),
        'singular_name' => __('Orvi Designers', 'post type singular name','orvi'),
        'add_new' => __('Add Orvi Designers','orvi'),
        'add_new_item' => __('Add Orvi Designers','orvi'),
        'edit_item' => __('Edit Orvi Designers','orvi'),
        'new_item' => __('New Orvi Designers','orvi'),
        'view_item' => __('View Orvi Designers','orvi'),
        'search_items' => __('Search Orvi Designers','orvi'),
        'not_found' =>  __('Nothing found','orvi'),
        'not_found_in_trash' => __('Nothing found in Trash','orvi'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
      ); 

    register_post_type( 'orvidesigners' , $args );
	
}

//////////////End Designers Post Type///////////

///////////remove add new option////////////

// hide "add new" on wp-admin menu
function hd_add_box() {
  global $submenu;
  unset($submenu['edit.php?post_type=enquiries'][10]);
}


add_action('admin_menu', 'hd_add_box');

function hd_add_buttons() {
  global $pagenow;
  if(is_admin()){
	if($pagenow == 'edit.php' && $_GET['post_type'] == 'enquiries'){
		echo '<style>.add-new-h2{display:none}</style>';
	}
  }
}
add_action('admin_head','hd_add_buttons');

///////////////
function filter_search($query) {
    if ($query->is_search) {
	$query->set('post_type', array('orvi_products'));
    };
    return $query;
};
add_filter('pre_get_posts', 'filter_search');

/*****************************************Add Enquiry fields***************************************************************/
function enquiries_add_meta_box() {



	$screens = array( 'enquiries' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'enquiries_sectionid',
			__( 'Enquiry Products', 'enquiries_textdomain' ),
			'enquiries_meta_box_callback',
			$screen
		);
	}
}
add_action( 'add_meta_boxes', 'enquiries_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function enquiries_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'enquiries_save_meta_box_data', 'enquiries_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$cust_name = get_post_meta( $post->ID, 'name', true );
	$cust_email = get_post_meta( $post->ID, 'email', true );
	$cust_phone = get_post_meta( $post->ID, 'phone', true );
	$cust_address = get_post_meta( $post->ID, 'address', true );
	//$cust_posts = get_post_meta( $post->ID, 'product_ids', true );
?>
<?php


echo '<label>';
	_e( 'Name :');
	echo '</label> ';
	echo '<label>'.$cust_name.'</label>';
	echo '<label>'."<br>";
	_e( 'Email :');
	echo '</label> ';
	echo '<label>'.$cust_email.'</label>';
	echo '<label>'."<br>";
	_e( 'Phone :');
	echo '</label> ';
	echo '<label>'.$cust_phone.'</label>';
	echo '<label>'."<br>";
	_e( 'Address :');
	echo '</label> ';
	echo '<label>'.$cust_address.'</label>'."<br>";
	
	echo '<label>';
	_e('Products Name(Select For Enquiry) :');
	echo '</label>'."<br>";
		$selected_post = get_post_meta(get_the_ID(), 'product_ids', true );
		$all_posts = explode(",",$selected_post);
		$all_posts = array_filter($all_posts);
	
		if(count($all_posts)>0){
		foreach($all_posts as $all_post){
		$sel_post = get_post($all_post);
		?>
        
        <p><?php echo $sel_post->post_title; ?></p>
		<?php }} ?>

<?php }

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function enquiries_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['enquiries_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['enquiries_meta_box_nonce'], 'enquiries_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset( $_POST['enquiries_new_field'] ) ) {
		return;
	}

	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['enquiries_new_field'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}
add_action( 'save_post', 'enquiries_save_meta_box_data' );


/*********************************************end ***************************************************************************/

/*********************************************Start related products ********************************************************/

/*function orvitheme_add_meta_box() {

		add_meta_box(
			'orvitheme_sectionid',
			__( 'Related Posts', 'orvitheme_textdomain' ),
			'orvitheme_add_meta_box_callback',
			"orvi_products"
		);
}
add_action( 'add_meta_boxes', 'orvitheme_add_meta_box' );

function orvitheme_add_meta_box_callback( $post ) {

	
	wp_nonce_field( 'orvitheme_meta_box', 'orvitheme_meta_box_nonce' );

	$value = get_post_meta( $post->ID, 'related_post_id', true );
	$myrelated_postid_allexp = explode(",", $value);
?>
	<div style="height:260px; overflow-y:scroll">
	<?php
		
		$args = array( 'post_type' => 'orvi_products', 'posts_per_page' =>-1,'order'=>'ASC' ,'post__not_in' => array( $post->ID ),'lang'=> $lang );
		
			$wpex_query = get_posts( $args );
			
			foreach($wpex_query as $single_post){
			?>
            <div>
            <p>
            <input type="checkbox" value="<?php echo $single_post->ID; ?>" <?php if(in_array($single_post->ID, $myrelated_postid_allexp)){ ?> checked="checked" <?php } ?> id="myrelated_postid" name="myrelated_postid[]" />
			<?php echo $same_post_title = $single_post->post_title; ?>
            </p>
            </div>
            
    <?php
	
			}
		
		?>
        </div>
<?php
}

function orvitheme_save_meta_box_data( $post_id ) {

	$post_type = $_POST['post_type'];

	if($post_type == 'orvi_products' ){

	$myrelated_postid_all = implode(",", $_REQUEST['myrelated_postid']);
	

	if(count($_REQUEST['myrelated_postid']> 0)){
		
		update_post_meta( $post_id, 'related_post_id', $myrelated_postid_all );
		
		}

	}
}
add_action( 'save_post', 'orvitheme_save_meta_box_data' );*/
/*********************************************End related products ********************************************************/

/**
 * Proper way to enqueue scripts and styles
 */
function orvitheme_scripts() {
	
	wp_enqueue_style( 'new-style', get_template_directory_uri().'/new-style.css' );
	//wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
	
}

add_action( 'admin_enqueue_scripts', 'orvitheme_scripts' );


//////////////////Add image size///////////////////////
add_action( 'after_setup_theme', 'setup' );
	function setup() {
    add_theme_support( 'post-thumbnails' ); // This feature enables post-thumbnail support for a theme
    add_image_size( 'featured-image', 480, 320, true);
	add_image_size( 'full-image', 965, 350, true );
	add_image_size( 'next-image', 675, 300, true );
}




// add category nicenames in body and post class
function category_id_class( $classes ) {
	global $post;
	//echo "<pre>";
	//print_r($classes); exit;
	if(in_array("home", $classes)){
		$classes[] = "homepage index-home index";
	}elseif(is_page("gallery", $classes)){
		$classes[] = "homepage index-home";
	}elseif(is_page("designers", $classes)){
		$classes[] = "homepage index-home";
	}elseif(is_page("blog", $classes)){
		$classes[] = "homepage index-home";
	}elseif(in_array("search", $classes)){
		$classes[] = "homepage index-home";
	}elseif(is_page("orvicat", $classes)){
		$classes[] = "index-category";
	}elseif(in_array("page", $classes)){
		$classes[] = "index-about";
	}elseif(in_array("archive", $classes)){
		$classes[] = "index-category";
	}elseif(in_array("single", $classes)){
		$classes[] = "index-single";
	}elseif(is_page("contact-us", $classes)){
		$classes[] = "index-about";
	}
	
	
	# 
	
	return $classes;
}
add_filter( 'body_class', 'category_id_class' );




function container_class(){
	global $post;
	
	$subclasses = array();
	if(is_front_page()){
		$subclasses[] = "intro-effect-grid";
	}elseif(is_archive()){
		$subclasses[] = "intro-effect-sidefixed modify";
	}	
	elseif(is_single()){
		$subclasses[] = "intro-effect-push";
	}
	elseif(is_page('gallery')){
		$subclasses[] = "intro-effect-push";
	}
	elseif(is_page('designers')){
		$subclasses[] = "intro-effect-push modify";
	}
	elseif(is_page('orvicat')){
		$subclasses[] = "intro-effect-sidefixed modify";
	}
	elseif(is_search()){
		$subclasses[] = "";
	}
	else{
		$subclasses[] = "intro-effect-side modify";
	}
	
	
	echo implode(" ", $subclasses);
}


function orvi_page_title(){
	global $post;
	if(is_front_page()){
		//echo "<h1>I want to travel the world</h1>";
	}elseif(is_archive()){
		echo '<h1>'.single_cat_title( '', false ).'</h1>';
		//echo '<p class="subline">My life is my message.</p>';
	}else{
		echo '<h1>'.get_the_title().'</h1>';
		//echo '<p class="subline">My life is my message.</p>';
	}
}
////////////////////////////Sidebar Widgets/////////////////////////////////
function orvitheme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Newletter', 'orvi' ),
		'id'            => 'newsletter',
		'description'   => __( 'Newletter', 'orvi' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Contact Address', 'orvi' ),
		'id'            => 'address',
		'description'   => __( 'Address', 'orvi' ),
		'before_widget' => '<span class="section-description">',
		'after_widget'  => '</span>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

}
add_action( 'widgets_init', 'orvitheme_widgets_init' );


/////////////////// Checkbox for category on home page///////////////////


// Add term page
function tutorialshares_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[custom_term_meta]"><?php _e( 'Select for Home Page', 'orvi' ); ?></label>
		<input type="checkbox" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="yes" <?php echo esc_attr( $term_meta['custom_term_meta'] ) == "yes" ? 'checked="checked"' : ''; ?> >
		<p class="description"><?php _e( 'Check this, if you want to show this category on home page','orvi' ); ?></p>
	</div>
<?php
}
add_action( 'orvicat_add_form_fields', 'tutorialshares_taxonomy_add_new_meta_field', 10, 2 );

function tutorialshares_taxonomy_edit_meta_field($term) { 

 // put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Select for Home Page', 'orvi' ); ?></label></th>
		<td>
			<input type="checkbox" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="yes" <?php echo esc_attr( $term_meta['custom_term_meta'] ) == "yes" ? 'checked="checked"' : ''; ?>>
			<p class="description"><?php _e( 'Check this, if you want to show this category on home page','orvi' ); ?></p>
		</td>
	</tr>
    
    
<?php
}
add_action( 'orvicat_edit_form_fields', 'tutorialshares_taxonomy_edit_meta_field', 10, 2 );


// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	$_POST['term_meta']['custom_term_meta'] = isset($_POST['term_meta']['custom_term_meta'])?$_POST['term_meta']['custom_term_meta']:'no';
	if ( isset( $_POST['term_meta'] )) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
	//print_r($term_meta);
}  
add_action( 'edited_orvicat', 'save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_orvicat', 'save_taxonomy_custom_meta', 10, 2 );








# Hari Code
add_action('init', 'orvi_init_callback');
function orvi_init_callback(){
	global $wpdb;
	
	$_set_range_parameter = isset($_REQUEST['_set_range_parameter'])?$_REQUEST['_set_range_parameter']:'';
	$range = isset($_REQUEST['range'])?$_REQUEST['range']:'';
	
	if(!empty($range) && !empty($_set_range_parameter)){
		session_start();
		$_SESSION['_range'] = $range;
		wp_redirect(home_url('/orvicat/'));
		exit;
	}
	
}
# set session for countries
add_action('init', 'country_init_callback');

define('CURRENT_URL', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

function country_init_callback(){
	global $wpdb;
	
	
	
	$_set_country_parameter = isset($_REQUEST['_set_country_parameter'])?$_REQUEST['_set_country_parameter']:'';
	$country = isset($_REQUEST['country'])?$_REQUEST['country']:'';
	
	if(!empty($country) && !empty($_set_country_parameter)){
		session_start();
		$_SESSION['_country'] = $country;
		wp_redirect(home_url('/orvicat/'));
		exit;
	}
	
	
	
	$cat = isset($_POST['cat'])?$_POST['cat']:'';
	$redirect_to = isset($_POST['redirect_to'])?$_POST['redirect_to']:'';
	$action = isset($_POST['action'])?$_POST['action']:'';
	if(!empty($cat) && $action=='changecountry'){
		session_start();
		$_SESSION['_country'] = $cat;
		wp_redirect($redirect_to);
		exit;
	}
	
}
/*********************************************end ***************************************************************************/

/////////////////IP Location///////////////////////
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}


///////////////////END//////////////////


function get_country(){
	$ip_country_code = ip_info("Visitor", "Country Code"); // IN
	$ip_country_code = $ip_country_code!=''?$ip_country_code:"IN";
	if(!isset($_SESSION['_country'])){
		$countryslug = strtolower($ip_country_code);
	}else{
		$countryslug = strtolower($_SESSION['_country']);
	}
	$category = get_term_by('slug', $countryslug, 'countries' );
	$catid = $category->term_id;
	
	
	return $catid;
}




