<?php 
/*
** Template name: Designers Page
*/
get_header('blog');
?>

<?php /*?>
<article style="float:left; width:35%;">      	
    <div>
        <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120"></a></div>
        <div class="v-range-align"><?php the_title(); ?></div>
    </div>
</article>

<article class="content no-padding color-x" style="float:right; width:65%;">
<div class="no-margin">
<div class="grid">
<?php 
					$args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
					$wpex_query = new WP_Query( $args );
					while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                ?>
                	<figure class="single-item-effect">
						<?php
                        if ( has_post_thumbnail()):
                        the_post_thumbnail('catalogue-featured-image');
                        endif;
                        ?> 
                        <figcaption>
                        	<div class="figcaption-border">
                        	<h2><?php the_title(); ?></h2>
                        	<a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                        	<div class="figure-overlay"></div>
                        	</div>
                        </figcaption>												
                    </figure>
    			<?php endwhile;wp_reset_postdata(); ?> 
</div>
</div>
</article>
<?php */?>


<?php if($_SESSION['_range'] == "x"){ ?>
    <article class="width_35">
        
        <div>
            <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120"></a></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/x-range.png" /></div>
            <div class="v-range-align"><?php the_title(); ?></div>
            <div class="v-range-align"><p><?php echo $categoryDesc->description; ?></p></div>
        </div>
        
        
        
    </article>
    <article class="content no-padding width_65">
				
				<div class="no-margin">
					<div class="grid">
					
					 <?php 
					$args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
					$wpex_query = new WP_Query( $args );
					while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                ?>
                	<figure class="single-item-effect big">
						<?php
                        /*if ( has_post_thumbnail()):
                        the_post_thumbnail('catalogue-featured-image');
                        endif;*/
                        ?> 
                        <?php if( get_field('profile_pic') ): ?>
                            
                            <img src="<?php the_field('profile_pic'); ?>" />
                        
                        <?php endif; ?>
                        <figcaption>
                        	<div class="figcaption-border">
                        	<h2><?php the_title(); ?></h2>
                        	<a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                        	<div class="figure-overlay"></div>
                        	</div>
                        </figcaption>												
                    </figure>
    			<?php endwhile;wp_reset_postdata(); ?>
                        
                      
					</div>
				</div>
			</article>
<?php } ?>

<?php if($_SESSION['_range'] == "v"){ ?>
    <article class="width_35">
        
        <div>
            <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="Orvi" width="120"></a></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-range.png" /></div>
            <div class="v-range-align"><?php the_title(); ?></div>
            <div class="v-range-align v-color-range-time"><p><?php echo $categoryDesc->description; ?></p></div>
        </div>
        
        
        
    </article>
    <article class="content no-padding width_65">
				
				<div class="no-margin">
					<div class="grid">
					
					<?php 
					$args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
					$wpex_query = new WP_Query( $args );
					while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                ?>
                	<figure class="single-item-effect big">
						<?php
                        /*if ( has_post_thumbnail()):
                        the_post_thumbnail('catalogue-featured-image');
                        endif;*/
                        ?> 
                        <?php if( get_field('profile_pic') ): ?>
                            
                            <img src="<?php the_field('profile_pic'); ?>" />
                        
                        <?php endif; ?>
                        <figcaption>
                        	<div class="figcaption-border">
                        	<h2><?php the_title(); ?></h2>
                        	<a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                        	<div class="figure-overlay"></div>
                        	</div>
                        </figcaption>												
                    </figure>
    			<?php endwhile;wp_reset_postdata(); ?>
                        
                      
					</div>
				</div>
			</article>
<?php } ?>

			

<?php get_footer(); ?>















 <?php /*?><article class="">
    	<div class="">
    	<div id="grid-gallery" class="grid-gallery">
    	<section class="grid-wrap">
        		<div class="grid" id="isotope-container">
				<?php 
					$args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
					$wpex_query = new WP_Query( $args );
					while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                ?>
                	<figure class="single-item-effect">
						<?php
                        if ( has_post_thumbnail()):
                        the_post_thumbnail('catalogue-featured-image');
                        endif;
                        ?> 
                        <figcaption>
                        	<div class="figcaption-border">
                        	<h2><?php the_title(); ?></h2>
                        	<a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                        	<div class="figure-overlay"></div>
                        	</div>
                        </figcaption>												
                    </figure>
    			<?php endwhile;wp_reset_postdata(); ?>
    
    
    
    		</div>
    	</section><!-- // grid-wrap -->
    
    	</div><!-- // grid-gallery -->
    	</div>
    </article><?php */?>
<?php get_footer(); ?>