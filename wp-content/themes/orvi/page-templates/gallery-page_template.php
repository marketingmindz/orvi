<?php
/*
** Template name:Gallery Page
*/
get_header();
?>  
			<article class="content">
				<div class="">
					<div id="grid-gallery" class="grid-gallery">
						<section class="grid-wrap">
							
                            <div class="grid" id="isotope-container">
								<?php 
									$z = 1;
									$x = 1;
									$args = array( 'post_type' => 'orvi_products', 'posts_per_page' =>-1,'order'=>'ASC');
									$wpex_query = new WP_Query( $args );
									while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
									
										if( $x%2 == 1 ){
											$class_home = 'small';
										}else{
											$class_home = 'big';
										}
										
										if($z%2==0) $x++;
								?>
                                    <figure class="single-item-effect <?php echo $class_home ?>">
                                        <?php
											if ( has_post_thumbnail()):
												the_post_thumbnail('catalogue-featured-image');
											endif;
										?> 
                                        <figcaption>
                                            <div class="figcaption-border">
                                                <h2><?php the_title(); ?></h2>
                                                <a href="<?php echo get_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                                                <div class="figure-overlay"></div>
                                            </div>
                                        </figcaption>												
                                    </figure>
                    			<?php $z++; endwhile;wp_reset_postdata(); ?>
                                
                                
						
							</div>
						</section><!-- // grid-wrap -->
						
					</div><!-- // grid-gallery -->
				</div>
			</article>
            
            
            
            
            <!-- Filter Button -->

		<!--<div id="filter-nav">
			<nav id="filter-main-nav">
				<ul>
					<li><a href="#" data-filter="*">All</a></li>
					<li><a href="#" data-filter=".wild">In the wild</a></li>
					<li><a href="#" data-filter=".city-break">City Break</a></li>
					<li><a href="#" data-filter=".paris">Paris</a></li>
					<li><a href="#" data-filter=".los-angeles">Los Angeles</a></li>
					<li><a href="#" data-filter=".las-vegas">Las Vegas</a></li>
					<li class="filter-title">Filters</li>
				</ul>
			</nav>
			<a href="#" class="filter-nav-trigger"><span class="fa fa-filter"></span></a>
		</div>-->

        
		
<?php get_footer(); ?>