<?php
/*
** Template name:Category Page
*/
get_header('blog');
?>  
          <?php if(isset($_SESSION['_range']) && $_SESSION['_range'] == "x"){ ?>
            <article class="width_35">
            	
                <div>
            <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120"></a></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/x-range.png" /></div>
            <div class="v-range-align"><p><?php echo get_option('wpc_xrange'); ?></p></div>
        </div>
                
                
                
            </article>
           	
			
            <article class="content no-padding color-x width_65">
                <div class="no-margin">
                	<div class="grid">
						<?php
						$term_id = 46;
						$taxonomy_name = 'orvicat';
						$termchildren = get_term_children( $term_id, $taxonomy_name );
						?>
                        <?php 
                        foreach ( $termchildren as $child ) {
							
							/*if( $i % 3 == 0 ){
								$class_name = 'full';
							}else{
								$class_name = 'big';
							}*/
							$term = get_term_by( 'id', $child, $taxonomy_name );
							
						?>
                        <a href="<?php echo get_term_link( $child, $taxonomy_name ); ?>">
                        <figure class="single-item-effect full five">
                        	<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
							<img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                       
							<figcaption>
								<div class="figcaption-border">
                                
									<h2><?php echo $term->name; ?></h2>
									
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
						</a>	
						
                        <?php /*$i++;*/ } ?> 
                    </div>
                </div>
            </article>
            <?php } ?>
            
            <?php if(isset($_SESSION['_range']) && $_SESSION['_range']  == "v"){ ?>
			<article class="width_35">
            	
                <div>
            <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="Orvi" width="120"></a></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-range.png" /></div>
            <div class="v-range-align"><p><?php echo get_option('wpc_vrange'); ?></p></div>
        </div>
                
                
                
            </article>
            <article class="content no-padding color-v width_65">
                <div class="no-margin">
                	<div class="grid">
					<?php
					$term_id = 47;
					$taxonomy_name = 'orvicat';
					$termchildren = get_term_children( $term_id, $taxonomy_name );
					?>
                     <?php 
                        foreach ( $termchildren as $child ) {
							
							/*if( $i % 3 == 0 ){
								$class_name = 'full';
							}else{
								$class_name = 'big';
							}*/
							$term = get_term_by( 'id', $child, $taxonomy_name );
							
						?>
                        <a href="<?php echo get_term_link( $child, $taxonomy_name ); ?>">
                        <figure class="single-item-effect big five">
                        	<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
							<img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                       
							<figcaption>
								<div class="figcaption-border">
                                
									<h2><?php echo $term->name; ?></h2>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
						</a>	
						
                        <?php  } ?>    
                    </div>
                </div>
            </article>
            <?php } ?>
<?php get_footer(); ?>