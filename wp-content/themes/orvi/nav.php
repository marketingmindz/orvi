<?php
require_once "../../../wp-load.php";
global $wpdb;
?>
    <div class="st-menu st-effect-1">
        <div class="fullwidth site-infos">
            <div class="logo">
                <a href="<?php bloginfo('url'); ?>">
                <img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120" />
                </a>
            </div>
    	</div>
        
        <nav class="fullwidth sidebar-navigation-menu">
            

		<ul>
			<li><a href="<?php bloginfo('url'); ?>"><?php /*?><i class="fa fa-home"></i><?php */?><?php _e('Home','orvi'); ?></a></li>
			
            <?php /* Category Menu Start */ ?>
            <?php /*if(is_front_page()){ ?>
            <li style="display:none;"><a href="<?php echo site_url('/orvicat/'); ?>"><i class="fa fa-pencil-square-o"></i><?php _e('Category','orvi'); ?></a>
            	<ul class="x-v-rangetab cate_sub_menu">
				<?php if($_SESSION['_range'] == "x"){ ?>
                <?php 
					$category = get_term_by('slug', 'x-range', 'orvicat' );
					$args = array(
					'show_option_all'    => '',
					'orderby'            => 'name',
					'order'              => 'ASC',
					'style'              => 'list',
					'child_of'           => $category->term_id,
					'exclude'            => '',
					'hierarchical'       => 1,
					'depth'              => 0,
					'title_li'		   => '',
					'taxonomy'           => 'orvicat',
					'walker'             => null
					);
					wp_list_categories( $args );
				?>
                <?php } elseif($_SESSION['_range'] == "v") { ?>
                <?php 
					$category = get_term_by('slug', 'v-range', 'orvicat' );
					$args = array(
					'show_option_all'    => '',
					'orderby'            => 'name',
					'order'              => 'ASC',
					'style'              => 'list',
					'child_of'           => $category->term_id,
					'exclude'            => '',
					'hierarchical'       => 1,
					'depth'              => 0,
					'title_li'		   => '',
					'taxonomy'           => 'orvicat',
					'walker'             => null
					);
					wp_list_categories( $args ); 
				?>
				<?php } ?>
                </ul>
            </li>
            <?php } else */ if($_SESSION['_range'] == "x" || $_SESSION['_range'] == "v" && !is_front_page()){ ?>
            <li><a href="<?php echo site_url('/orvicat/'); ?>"><?php _e('Category','orvi'); ?></a>
            	<ul class="x-v-rangetab cate_sub_menu">
				<?php if($_SESSION['_range'] == "x"){ ?>
                <?php 
					$category = get_term_by('slug', 'x-range', 'orvicat' );
					$args = array(
					'show_option_all'    => '',
					'orderby'            => 'name',
					'order'              => 'ASC',
					'style'              => 'list',
					'child_of'           => $category->term_id,
					'exclude'            => '',
					'hierarchical'       => 1,
					'depth'              => 0,
					'title_li'		   => '',
					'taxonomy'           => 'orvicat',
					'walker'             => null
					);
					wp_list_categories( $args );
				?>
                <?php } elseif($_SESSION['_range'] == "v") { ?>
                <?php 
					$category = get_term_by('slug', 'v-range', 'orvicat' );
					$args = array(
					'show_option_all'    => '',
					'orderby'            => 'name',
					'order'              => 'ASC',
					'style'              => 'list',
					'child_of'           => $category->term_id,
					'exclude'            => '',
					'hierarchical'       => 1,
					'depth'              => 0,
					'title_li'		   => '',
					'taxonomy'           => 'orvicat',
					'walker'             => null
					);
					wp_list_categories( $args ); 
				?>
				<?php } ?>
                </ul>
            </li>
            <?php } ?>
			<?php /* Category Menu End */ ?>
            
            
            <?php /* Switch to X-V Range Start */ ?>
            
            <?php if(isset($_SESSION['_range']) && $_SESSION['_range'] == "x"){ ?>
            <li><a href="<?php echo home_url('?_set_range_parameter=1&range=v'); ?>"><?php /*?><i class="fa fa-cubes"></i><?php */?><?php _e('V Range','orvi'); ?></a></li>
            <?php } elseif(isset($_SESSION['_range']) && $_SESSION['_range'] == "v") { ?>
            <li><a href="<?php echo home_url('?_set_range_parameter=1&range=x'); ?>"><?php _e('X Range','orvi'); ?></a></li>	
            <?php } else { ?>
            <li><a href="<?php echo home_url('?_set_range_parameter=1&range=x'); ?>"><?php _e('X Range','orvi'); ?></a></li>
            <li><a href="<?php echo home_url('?_set_range_parameter=1&range=v'); ?>"><?php _e('V Range','orvi'); ?></a></li>
            <?php } ?>
            
            <?php /* Switch to X-V Range End */ ?>
            
            
            <?php /* Country Dropdown Start   ?>
			<li>
				<form action="" method="post" id="changecountry">
					<?php
                        $categories = get_terms('countries');
                        //$actual_link = CURRENT_URL;
                        echo '<input type="hidden" name="redirect_to" id="redirect_to" value="" />';
						echo '<input type="hidden" name="action" value="changecountry" />';
                        $select = "<select name='cat' id='cat' class='postform'>\n";
                        $select.= "<option value='-1'>Select Country</option>\n";
                        
                        foreach($categories as $category){
                            if($category->count > 0){
                                $select.= "<option value='".$category->slug."'>".$category->name."</option>";
                            }
                        }
                        
                        $select.= "</select>";
                        
                        echo $select;
                        ?>
                        <script type="text/javascript">
							var redirecturl = (window.location != window.parent.location)
							? document.referrer
							: document.location;
							
							document.getElementById("redirect_to").value = redirecturl;
                            var dropdown = document.getElementById("cat");
                            function onCatChange() {
                            	document.getElementById("changecountry").submit(); 
                            }
                            dropdown.onchange = onCatChange;
                        </script>
           		</form>
            </li>
            <?php  Country Dropdown End */ ?>
            
            
            <?php /* Different About Us Page For X and V Range Start */ ?>
            
            <?php if($_SESSION['_range'] == "x"){ ?>
			<li><a href="<?php echo site_url('/about-us-x/'); ?>"><?php /*?><i class="fa fa-heart"></i><?php */?><?php _e('About Us','orvi'); ?></a></li>
            <?php } elseif($_SESSION['_range'] == "v") { ?>
            <li><a href="<?php echo site_url('/about-us-v/'); ?>"><?php _e('About Us','orvi'); ?></a></li>
            <?php } ?>
            
            <?php /* Different About Us Page For X and V Range End */ ?>
            
            <?php /* Designers tab only for X-Range Start ?>
            <?php if($_SESSION['_range'] == "x"){ ?>
			<li><a href="<?php echo site_url('/designers/'); ?>"><i class="fa fa-star"></i><?php _e('Designers','orvi'); ?></a>
                <ul class="x-v-rangetab dies_sub_menu">
                <?php
                    $args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
                    $wpex_query = new WP_Query( $args );
                    while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                ?>
                    <li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile;wp_reset_postdata(); ?>
                </ul>
            </li>
            <?php } else {?>
            <li style="display:none;"><a href="<?php echo site_url('/designers/'); ?>"><i class="fa fa-star"></i><?php _e('Designers','orvi'); ?></a>
                <ul class="x-v-rangetab dies_sub_menu">
                <?php
                    $args = array( 'post_type' => 'orvidesigners', 'posts_per_page' =>-1,'order'=>'ASC');
                    $wpex_query = new WP_Query( $args );
                    while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
                ?>
                    <li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile;wp_reset_postdata(); ?>
                </ul>
            </li>
            <?php } ?>
            <?php Designers tab only for X-Range End */ ?>
            
            <li><a href="<?php echo site_url('/blog/'); ?>"><?php _e('Blog','orvi'); ?></a></li>
            <?php /*?><li><a href="<?php echo site_url('/where-to-buy/'); ?>"><i class="fa fa-folder-o"></i><?php _e('Where to Buy','orvi'); ?></a></li>
            <li><a href="<?php echo site_url('/media-center/'); ?>"><i class="fa fa-pencil-square-o"></i><?php _e('Media center','orvi'); ?></a></li><?php */?>
            
            
            <?php /* Different Installation Page For X and V Range Start  ?>
            <?php if($_SESSION['_range'] == "x"){ ?>
            <li><a href="<?php echo site_url('/installation-x/'); ?>"><i class="fa fa-gears"></i><?php _e('Installation','orvi'); ?></a></li>
            <?php } elseif($_SESSION['_range'] == "v") { ?>
            <li><a href="<?php echo site_url('/installation-v/'); ?>"><i class="fa fa-gears"></i><?php _e('Installation','orvi'); ?></a></li>
            <?php } ?>
			<?php  Different Installation Page For X and V Range End */ ?>
            
            
            
            <li><a href="<?php echo site_url('/contact-us/'); ?>"><?php _e('Contact','orvi'); ?></a></li>
		</ul>
	</nav>
	<div class="fullwidth newsletter">
		<div id="mc_embed_signup">
			<h4 class="section-title"><?php _e('Newsletter','orvi'); ?></h4>
			<?php echo do_shortcode('[email-newsletter-plugin]');?>
            
		</div>
	</div>
    <div class="fullwidth contact-add">
        <div id="mc_embed_signup">
        	<?php dynamic_sidebar('address');?>
        </div>
    </div>
	<!--<div class="menu_bottom">
    	<ul>
        	<li>Terms &amp; Conditions</li>
            <li>Privacy Policy</li>
        </ul>
    </div>-->
	<div class="fullwidth sidebar-social-networks contact-add">
		<h4>Connect</h4>
        <ul>
			<li class="facebook"><a href="https://www.facebook.com/OrviTiles"><i class="fa fa-facebook"></i></a></li>
			<li class="twitter"><a href="https://twitter.com/orvitiles"><i class="fa fa-twitter"></i></a></li>
			<li class="googleplus"><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
			<?php /*?><li class="youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
			<li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
			<li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li><?php */?>
		</ul>
	</div>
	<div class="fullwidth bottom-links">
    	<p>2015 © Orvi. All rights reserved.</p>
	</div>
</div>