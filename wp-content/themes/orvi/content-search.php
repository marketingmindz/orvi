<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
 get_header();
?>

<?php
									
					$postid = get_the_ID();
					$get_product_categories = get_the_terms($post->ID, 'orvicat' );
						if(!empty($get_product_categories)){
						foreach($get_product_categories as $get_product_category){
						//print_r($get_product_category);					
						$product_nmae = $get_product_category->name;
						$product_cate_id = $get_product_category->term_id;
						$product_slug = $get_product_category->slug;
						$productparentcate =  $get_product_category->parent;
						
						
                ?>             	
                    <figure class="single-item-effect big" id="post-<?php the_ID(); ?>">
						<?php echo get_the_post_thumbnail( $page->ID, 'large' ); ?>
                            <figcaption>
                                <div class="figcaption-border">
                                    <h2><?php the_title(); ?><?php the_title(); ?></h2>
                                    	<?php //the_content();  ?>
                                    	<a href="<?php echo esc_url( get_permalink() ); ?>"><?php _e('View more'); ?></a>
                                    <div class="figure-overlay"></div>
                                </div>
                            </figcaption>												
                    </figure>
                    
                <?php 
					}
					}
                ?>
                
			
<?php //get_footer(); ?>