<form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url()?>/search" enctype="multipart/form-data">
    
    	<select id="product_list" name="product_list">
        	<option value="">Product List</option>
        	 <?php 
				//$args = array( 'post_type' => 'orvi_products', 'posts_per_page' =>-1,'order'=>'ASC','post_parent' => 47,  );
					$args =  array(
					'post_type' => 'orvi_products',
					'showposts' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'orvicat',
							'terms' => 47,
							'field' => 'term_id',
						)
					),
					'orderby' => 'title',
					'order' => 'ASC' )
;
				
				$wpex_query = new WP_Query( $args );
				$array_products = array();
				while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
				if(!in_array(get_the_title(), $array_products)){
					$array_products[] = get_the_title();
				 ?>
					<option value="<?php the_title(); ?>" <?php if($_REQUEST['product_list']==get_the_title()){ ?>selected<?php } ?>><?php the_title(); ?></option>
                   <?php } ?>
				<?php endwhile;wp_reset_postdata(); ?>
        </select>
        
        
        <select id="product_type" name="product_type">
        	<option value="">Product Type</option>
        	<?php $psql = "SELECT DISTINCT `meta_value` FROM " . $wpdb->prefix . "postmeta as pm 
			INNER JOIN " . $wpdb->prefix . "term_relationships as tr ON pm.post_id=tr.object_id 
			INNER JOIN " . $wpdb->prefix . "term_taxonomy as tt ON tr.term_taxonomy_id=tt.term_id 
			WHERE tt.parent='47' AND `meta_key`= 'type' AND `meta_value`!= ''";       
			$types = $wpdb->get_results($psql);
			
			foreach($types as $type){
			 ?>
            	<option value="<?php echo $type->meta_value; ?>" <?php if($_REQUEST['product_type']==$type->meta_value){ ?>selected<?php } ?>><?php echo $type->meta_value; ?></option>
            <?php } ?>
        </select>
        
        <select id="product_finish" name="product_finish">
        	<option value="">Finish</option>
        	<?php $sql = "SELECT DISTINCT `meta_value` FROM " . $wpdb->prefix . "postmeta as pm 
			INNER JOIN " . $wpdb->prefix . "term_relationships as tr ON pm.post_id=tr.object_id 
			INNER JOIN " . $wpdb->prefix . "term_taxonomy as tt ON tr.term_taxonomy_id=tt.term_id 
			WHERE tt.parent='47' AND `meta_key` = 'finish' AND `meta_value` != ''";
			$finishs = $wpdb->get_results($sql);
			
			foreach($finishs as $finish){
			 ?>
            	<option value="<?php echo $finish->meta_value; ?>" <?php if($_REQUEST['product_finish']==$finish->meta_value){ ?>selected<?php } ?>><?php echo $finish->meta_value; ?></option>
            <?php } ?>
        </select>
        
        <select id="product_sizes" name="product_sizes">
        	<option value="">Sizes</option>
        	<?php $sql1 = "SELECT DISTINCT `meta_value` FROM " . $wpdb->prefix . "postmeta as pm 
			INNER JOIN " . $wpdb->prefix . "term_relationships as tr ON pm.post_id=tr.object_id 
			INNER JOIN " . $wpdb->prefix . "term_taxonomy as tt ON tr.term_taxonomy_id=tt.term_id 
			WHERE tt.parent='47' AND `meta_key` = 'sizes' AND `meta_value` != ''";
			$sizes = $wpdb->get_results($sql1);
			
			foreach($sizes as $size){
			 ?>
            	<option value="<?php echo $size->meta_value; ?>" <?php if($_REQUEST['product_sizes']==$size->meta_value){ ?>selected<?php } ?>><?php echo str_replace('<br/>',',',$size->meta_value); ?></option>
            <?php } ?>
        </select>
        <select id="product_color" name="product_color">
        	<option value="">Color Variation</option>
        	<?php $sql2 = "SELECT DISTINCT `meta_value` FROM " . $wpdb->prefix . "postmeta as pm 
			INNER JOIN " . $wpdb->prefix . "term_relationships as tr ON pm.post_id=tr.object_id 
			INNER JOIN " . $wpdb->prefix . "term_taxonomy as tt ON tr.term_taxonomy_id=tt.term_id 
			WHERE tt.parent='47' AND `meta_key` = 'colour_variation' AND `meta_value` != ''";
			$colour_variations = $wpdb->get_results($sql2);
			
			foreach($colour_variations as $colour_variation){
			 ?>
            	<option value="<?php echo $colour_variation->meta_value; ?>" <?php if($_REQUEST['product_color']==$colour_variation->meta_value){ ?>selected<?php } ?>><?php echo $colour_variation->meta_value; ?></option>
            <?php } ?>
        </select>
        
        <select id="product_application" name="product_application">
        	<option value="">Product Application</option>
        	<?php $sql2 = "SELECT DISTINCT `meta_value` FROM " . $wpdb->prefix . "postmeta as pm 
			INNER JOIN " . $wpdb->prefix . "term_relationships as tr ON pm.post_id=tr.object_id 
			INNER JOIN " . $wpdb->prefix . "term_taxonomy as tt ON tr.term_taxonomy_id=tt.term_id 
			WHERE tt.parent='47' AND `meta_key` = 'application' AND `meta_value` != ''";
			$colour_variations = $wpdb->get_results($sql2);
			
			foreach($colour_variations as $colour_variation){
			 ?>
            	<option value="<?php echo $colour_variation->meta_value; ?>" <?php if($_REQUEST['product_application']==$colour_variation->meta_value){ ?>selected<?php } ?>><?php echo $colour_variation->meta_value; ?></option>
            <?php } ?>
        </select>
        
        <input type="submit" value="search" name="ads" id="ads" />
        
    </form>