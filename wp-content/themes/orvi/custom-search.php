<?php
/**
* Template Name: Custom Search	
*/
get_header('blog');

?>

	<article class=" width_35">
        <div>
            <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="Orvi" width="120"></a></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-range.png" /></div>
            <div class="v-range-align"><?php the_title(); ?></div>
            <div class="v-range-align"><p><?php echo $categoryDesc->description; ?></p></div>
        </div>
    </article>





<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////?>
    <article class="content no-padding width_65">
        <div class="no-margin">
            <div class="grid">
            <?php if(isset($_REQUEST['ads'])){
				$product_list = $_REQUEST['product_list'];
				$product_type = $_REQUEST['product_type'];
				$product_finish = $_REQUEST['product_finish'];
				$product_color = $_REQUEST['product_color'];
				$product_size = $_REQUEST['product_sizes'];
				$product_app = $_REQUEST['product_application'];
				
				if($product_list =='' && $product_type =='' && $product_finish =='' && $product_color =='' && $product_size =='' && $product_app ==''){ ?>
					<h3><?php _e('No result found','orvi'); ?></h3>
				<?php }else{
					$args =  array(
						'orderby'=> 'title',
						'order'=> 'ASC',
						'post_type' => 'orvi_products',
						'a' => $product_list,
						'posts_per_page' => -1
					);
			
					$args['tax_query'][]=array(
							'taxonomy' => 'orvicat',
							'field'    => 'term_id',
							'terms'    => '47',		
						);
			
					if($product_type!='' && $product_type!='0'){
						$args['meta_query'][]=array(
							'key'     => 'type',
							'value'   => $product_type,
							'compare' => 'LIKE',		
						);
					}
			
					if($product_finish!='' && $product_finish!='0'){
						$args['meta_query'][]=array(
							'key'     => 'finish',
							'value'   => $product_finish,
							'compare' => 'LIKE',		
						);
					}
			
					if($product_color!='' && $product_color!='0'){
						$args['meta_query'][]=array(
							'key'     => 'colour_variation',
							'value'   => $product_color,
							'compare' => 'LIKE',		
						);
					}
			
			
					if($product_size!='' && $product_size!='0'){
						$args['meta_query'][]=array(
							'key'     => 'sizes',
							'value'   => $product_size,
							'compare' => 'LIKE',		
						);
					}
			
					if($product_app!='' && $product_app!='0'){
						$args['meta_query'][]=array(
							'key'     => 'application',
							'value'   => $product_app,
							'compare' => 'LIKE',		
						);
					}
			
				 
			
					$loop = new WP_Query();
					$loop->query($args);
					
					$counter=1;    
			
				if ( $loop->have_posts() ) { ?>
				
				<?php while ( $loop->have_posts() ) :$loop->the_post(); ?>
				<a href="<?php echo get_permalink(); ?>">
				<figure class="single-item-effect big five">
					
					<?php if( get_field('image_for_category_page') ): ?>
                            
                        <img src="<?php the_field('image_for_category_page'); ?>" />
                    
                    <?php endif; ?>
					<?php //echo get_the_post_thumbnail( $page->ID, 'large' ); ?>
					<figcaption>
						<div class="figcaption-border">
						<h2><?php the_title(); ?></h2>
						
						<div class="figure-overlay"></div>
						</div>
					</figcaption>
				</figure>
				</a>
				<?php endwhile; } else { ?>
				<h3><?php _e('No result found','orvi'); ?></h3>
				<?php } 
			} } ?>
            
            
            </div>
        </div>
    </article>
<?php ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////?>


<?php get_footer(); ?>

