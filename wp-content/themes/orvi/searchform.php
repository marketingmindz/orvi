<form role="search" action="<?php echo home_url('/'); ?>" method="get" id="searchform">
<input class="travelogue-search-input" placeholder="Enter your search term..." value="<?php echo get_search_query(); ?>" name="s" id="s">
<input class="travelogue-search-submit" type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>"/>
<span class="travelogue-icon-search fa fa-search"></span>
</form>