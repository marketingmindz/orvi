<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize.css" />
	<?php if($_SESSION['_range'] == "v"){ ?>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles_v.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize_v.css" />
	<?php } ?>
    <!-- CSS: Normalize -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" /><!-- CSS: Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css" />
    <?php wp_head(); ?>
    
</head>



<body <?php body_class(); ?>>

<div id="container" class="container">
	
	
       	<?php if($_SESSION['_range'] == "x"){ ?>
        <div id="travelogue-search" class="travelogue-search">     
			<?php get_search_form(); ?>
        </div>
        <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
        	<span class="cd-menu-icon"></span>
        </a>
        <?php } ?>
        <?php if($_SESSION['_range'] == "v"){ ?>
        <ul id="aps" class="">
            <li class="travelogue-icon-search fa fa-search"></li>
        </ul>
        <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
        	<span class="cd-menu-icon"></span>
        </a>
        <?php } ?>
        <script>
			jQuery(document).ready(function(){
			jQuery("#aps").click(function(){
			jQuery("#tog-aps").toggle('slow');
			});
			});
		</script>
        <div class="v-search" id="tog-aps" style="display:none;">
    		<?php require_once 'advance-search-form.php' ?>
		</div>
<script type="text/javascript">
	jQuery(function(){
		var var1 = setInterval(function(){
			jQuery(".fullwidth.sidebar-navigation-menu li ul").before("<div class='mobile_icon'><span></span></div>");
				
			clearInterval(var1);	
		}, 1500);
		
		jQuery(document).on('click', ".fullwidth.sidebar-navigation-menu li .mobile_icon", function(){
			jQuery(".fullwidth.sidebar-navigation-menu li ul").slideToggle();
		});
		
	});
</script>