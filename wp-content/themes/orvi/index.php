<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
	<article class="width_35">
        <div>
            <div class="v-range-align"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120"></a></div>
            <div class="v-range-align"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/x-range.png" /></div>
            <div class="v-range-align"><?php _e('Search'); ?></div>
        </div>
    </article>
    
    <article class="content no-padding width_65">
        <div class="no-margin">
            <div class="grid">
            
            
                <?php 
				$search_value = $_GET['s'];
				$args = array(
				'post_type' => 'orvi_products',
			    's' =>$search_value,
					'posts_per_page' => -1
				);				
				
				$args['tax_query'][]=array(
							'taxonomy' => 'orvicat',
							'field'    => 'term_id',
							'terms'    => '46',		
						);
				
				
				$query = new WP_Query( $args );
				
				if($query->post_count==0){
					
					$args = '';
					
					
					$args = array(
				'post_type' => 'orvi_products',
			   	'posts_per_page' => -1
				);	
				$args['tax_query'][]=array(
							'taxonomy' => 'orvicat',
							'field'    => 'term_id',
							'terms'    => '46',		
						);
					

					$args['meta_query']['relation'] = 'OR';
				    $args['meta_query'][]=array(
					'key'     => 'type',
					'value'   => $search_value,
					'compare' => 'LIKE',		
				);
				
				$args['meta_query'][]=array(
					'key'     => 'orvi_pro_cats',
					'value'   => $search_value,
					'compare' => 'LIKE',		
				);
	
				$args['meta_query'][]=array(
					'key'     => 'finish',
					'value'   => $search_value,
					'compare' => 'LIKE',		
				);
	
				$args['meta_query'][]=array(
					'key'     => 'colour_variation',
					'value'   => $search_value,
					'compare' => 'LIKE',		
				);
	
	
				$args['meta_query'][]=array(
					'key'     => 'sizes',
					'value'   => $search_value,
					'compare' => 'LIKE',		
				);
	
				$args['meta_query'][]=array(
					'key'     => 'application',
					'value'   => $search_value,
					'compare' => 'LIKE',		
				);
					}
					$query = new WP_Query( $args );
				?>

                <h3><?php _e('Search Result for :','orvi'); ?> <?php echo "$s"; ?> </h3>  
                
                <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) :$query->the_post(); ?>   
                    <a href="<?php echo get_permalink(); ?>">
                        <figure class="single-item-effect full five">
                            
							<?php if( get_field('image_for_category_page') ): ?>
                            
                                <img src="<?php the_field('image_for_category_page'); ?>" />
                            
                            <?php endif; ?>
                            
                            <figcaption>
                                <div class="figcaption-border">
                                <h2><?php the_title(); ?></h2>
                                
                                <div class="figure-overlay"></div>
                                </div>
                            </figcaption>
                        </figure>
                    </a>
                <?php endwhile; else: ?>
                    <h3><?php _e('No result found','orvi'); ?></h3>
                <?php endif; ?>
            </div>
        </div>
    </article>
		

<?php get_footer(); ?>