<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */

get_header(); ?>

			<article class="content no-padding">
				
				<div class="no-margin">
					<div class="grid">
					
					<?php $a = 1; ?>
                    
						
                        <?php 
						while ( have_posts() ) : the_post();
							if( $a % 3 == 0 ){
								$class_names = 'full';
							}else{
								$class_names = 'big';
							}
						?>
                        
                        <figure class="single-item-effect <?php echo $class_names; ?>">
                            <?php
								if ( has_post_thumbnail()):
									the_post_thumbnail('featured-image');
								endif;
							?>  
                              
                            <figcaption>
                                <div class="figcaption-border">
                                    <h2><?php the_title(); ?></h2>
                                    <a href="<?php the_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                                    <div class="figure-overlay"></div>
                                </div>
                            </figcaption>												
                        </figure>
                        <?php $a++; endwhile; ?>
                         
                        
                      
					</div>
				</div>
			</article>

<?php get_footer(); ?>

