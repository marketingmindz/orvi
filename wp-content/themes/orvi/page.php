<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header();
?>

			<article class="content">
				<div class="w-a-m">
					<?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
				</div>
				<?php /*?><!-- Map holder -->
				<div class="travelogue-product travelogue-container">
				    <div class="travelogue-product-wrapper">
				    	<!-- Map image -->
				        <img src="<?php bloginfo('template_url'); ?>/img/map.jpg" />

				        <!-- ul with all the points -->
				        <ul>
				            <!-- Single point -->
				            <li class="travelogue-single-point">
				                <a class="travelogue-img-replace" href="#0"><?php _e('More','orvi'); ?></a>

				                <div class="travelogue-more-info travelogue-left">
				                    <h2><?php _e('The Balkans','orvi'); ?></h2>

				                    <p><?php _e('Oh, you have to love the balkan accent. I bet you, you will
				                    love more their beautifull nature.','orvi'); ?></p><img src=
				                    "http://placehold.it/560x290/7f8c8d/ffffff" /> <a href="#0" class=
				                    "travelogue-close-info travelogue-img-replace">Close</a>
				                </div>
				            </li><!-- End of .travelogue-single-point -->

				            <!-- Single point -->
				            <li class="travelogue-single-point">
				                <a class="travelogue-img-replace" href="#0"><?php _e('More','orvi'); ?></a>

				                <div class="travelogue-more-info travelogue-right">
				                    <h2><?php _e('In the jungle','orvi'); ?></h2>

				                    <p><?php _e('The last time I was in the jungle, a monkey jumped me and
				                    stole my camera.','orvi'); ?></p><img src="http://placehold.it/560x290/7f8c8d/ffffff" />
				                    <a href="#0" class=
				                    "travelogue-close-info travelogue-img-replace"><?php _e('Close','orvi'); ?></a>
				                </div>
				            </li><!-- End of .travelogue-single-point -->

				            <!-- Single point -->
				            <li class="travelogue-single-point">
				                <a class="travelogue-img-replace" href="#0"><?php _e('More','orvi'); ?></a>

				                <div class="travelogue-more-info travelogue-right">
				                    <h2><?php _e('Murica','orvi'); ?></h2>

				                    <p><?php _e('The wild wild west has calmed down a little bit. It is more
				                    like calm calm west.','orvi'); ?></p><img src="http://placehold.it/560x290/7f8c8d/ffffff" />
				                    <a href="#0" class=
				                    "travelogue-close-info travelogue-img-replace"><?php _e('Close','orvi'); ?></a>
				                </div>
				            </li><!-- End of .travelogue-single-point -->

				        	<!-- Single point -->
				            <li class="travelogue-single-point">
				                <a class="travelogue-img-replace" href="#0"><?php _e('More','orvi'); ?></a>

				                <div class="travelogue-more-info travelogue-left">
				                    <h2><?php _e('Hot as Ice','orvi'); ?></h2>

				                    <p><?php _e('If hell was a real place, I bet this would be its capital.
				                    Temperatures here can reach minus 55 degrees.','orvi'); ?></p><img src=
				                    "http://placehold.it/560x290/7f8c8d/ffffff" /> <a href="#0" class=
				                    "travelogue-close-info travelogue-img-replace"><?php _e('Close','orvi'); ?></a>
				                </div>
				            </li><!-- End of .travelogue-single-point -->

				            <!-- Single point -->
				            <li class="travelogue-single-point">
				                <a class="travelogue-img-replace" href="#0"><?php _e('More','orvi'); ?></a>

				                <div class="travelogue-more-info travelogue-top">
				                    <h2><?php _e('Australia','orvi'); ?></h2>

				                    <p><?php _e('Everything is upside down but the blood will not rush down to
				                    your head. And there will be kangaroos.','orvi'); ?></p><img src=
				                    "http://placehold.it/560x290/7f8c8d/ffffff" /> <a href="#0" class=
				                    "travelogue-close-info travelogue-img-replace"><?php _e('Close','orvi'); ?></a>
				                </div>
				            </li><!-- End of .travelogue-single-point -->

				            <!-- Single point -->
				            <li class="travelogue-single-point">
				                <a class="travelogue-img-replace" href="#0"><?php _e('More','orvi'); ?></a>

				                <div class="travelogue-more-info travelogue-bottom">
				                    <h2><?php _e('Bearland','orvi'); ?></h2>

				                    <p><?php _e('If you love bears, go there but be warned they will not love
				                    you.','orvi'); ?></p><img src="http://placehold.it/560x290/7f8c8d/ffffff" /> <a href="#0" class=
				                    "travelogue-close-info travelogue-img-replace"><?php _e('Close','orvi'); ?></a>
				                </div>
				            </li><!-- End of .travelogue-single-point -->

				        </ul><!-- End of ul with all the points -->
				    </div><!-- End of .travelogue-product-wrapper -->
				</div><!-- End of Map holder -->

				<div class="clearfix"></div>

				<div>
					<h1 class="text-left"><span class="black"><?php _e('Let me take you ','orvi'); ?></span><span class="green"><?php _e('to the mountain','orvi'); ?></span></h1>
					<div class="author_skills">
			            <div class="one_half">
							<iframe src="http://www.youtube.com/embed/C-y70ZOSzE0?hd=1&amp;rel=0&amp;autohide=1&amp;showinfo=0" width="410" height="230" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
			            </div>
						<div class="one_half">
							<p><?php _e('Uniquely whiteboard just in time sources through fully researched methodologies. Rapidiously administrate vertical functionalities whereas robust outsourcing. Rapidiously exploit team building benefits before multimedia based portals. Quickly revolutionize seamless infomediaries without seamless quality vectors. Energistically productivate virtual bandwidth whereas pandemic niches. Interactively fabricate cost effective web services.','orvi'); ?></p>
						</div>
						<div class="divider_30"></div>
			          </div>
				</div>

				<div class="clearfix"></div>

				<!-- Tweets -->
				<div class="author_tweets_full white">
					<div class="boxed">
						<div class="one_sixth text-center">
							<i class="fa fa-twitter"></i>
						</div>
						<div class="sixth_one">
							<span><?php _e('@modeltheme - ','orvi'); ?><i><?php _e('This is the best theme in the world #html5 #themeforest/item/travelogue... http://t.co/modeltheme - 3 days ago','orvi'); ?></i></span>
						</div>
					</div>
				</div><?php */?>

				
			</article>
		

<?php get_footer(); ?>
