<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
    
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize.css" />
    <?php if($_SESSION['_range'] == "v"){ ?>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles_v.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize_v.css" />
    <?php } ?>
   
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" /><!-- CSS: Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css" />
    <?php wp_head(); ?>
    
</head>

<?php if(is_front_page()){ ?>
<style>
.container {
    background-color: #000;
}
</style>
<?php } ?>


<?php if($_SESSION['_range'] == "x" && is_page('orvicat')){ ?>
<style>
	body{
		background:#000 !important;
	}
.header.v-range-block .bg-img {
		background-color:#000 !important;
	}
	.intro-effect-sidefixed .header.v-range-block .bg-img::after{ background:#000 !important; }
	
	
	.index-category .intro-effect-sidefixed.modify .title h1 {
		color:#fff;
		margin-top: 25px;
	}
	
	.intro-effect-sidefixed .bg-img::before,
	.intro-effect-sidefixed .bg-img::after {
		content: '';
		position: absolute;
		z-index: 100;
	}

	.intro-effect-sidefixed .bg-img {
		background: #000 !important;
	}
		
	.intro-effect-sidefixed.modify .bg-img::after {
		-webkit-transform: translateX(0);
		transform: translateX(0);
		background-color: black;
	}

</style>
<?php } elseif($_SESSION['_range'] == "v" && is_page('orvicat')) { ?>
<style>
	.bg-img {
		background-color:#fff;
	}
	body{
		background-color:#fff;
	}
	.index-single #container > .title, .index-single #container > article{
		background-color:#fff;
	}
	
	.index-category .intro-effect-sidefixed.modify .title h1 {
		color:#000;
		margin-top: 25px;
	}	
	
	.intro-effect-sidefixed.modify .bg-img::after {
		-webkit-transform: translateX(0);
		transform: translateX(0);
		background-color: black;
	}

.color-v h2{ color:#000; }


</style>
<?php } ?>
<body <?php body_class(); ?>>

<div id="container" class="container <?php container_class(); ?>">
		<?php /*?><?php if($_SESSION['_range'] == "x"){ ?>
		<div id="travelogue-search" class="travelogue-search">     
			<?php get_search_form(); ?>
        </div>
        <?php } elseif($_SESSION['_range'] == "v"){ ?>
        <ul id="aps" class="">
            <li class="travelogue-icon-search fa fa-search"></li>
        </ul>
        <?php } ?><?php */?>
        <?php if($_SESSION['_range'] == "x"){ ?>
        <div id="travelogue-search" class="travelogue-search">     
			<?php get_search_form(); ?>
        </div>
        
        <?php } ?>
        <?php if($_SESSION['_range'] == "v"){ ?>
        <ul id="aps" class="bd">
            <li class="travelogue-icon-search fa fa-search"></li>
        </ul>
        <?php /*?><a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
        	<span class="cd-menu-icon"></span>
        </a><?php */?>
        <?php } ?>
       <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
        	<span class="cd-menu-icon"></span>
        </a>
      
                 
    <?php //echo do_shortcode('[awsqf-form id=686]'); ?>
	<script>
	jQuery(document).ready(function(){
		jQuery("#aps").click(function(){
		jQuery("#tog-aps").toggle('slow');
		});
	});
	</script> 
<div class="v-search" id="tog-aps" style="display:none;">
    <?php require_once 'advance-search-form.php' ?>
</div>

    <?php if(is_search()){ ?>
    
    
<header class="header" style="display:none;">

    <div class="bg-img">
		<?php
        	if(is_archive()){
        	if (function_exists('z_taxonomy_image_url')) $texonomy_image_url = z_taxonomy_image_url(); ?>
        	<img style="display:none;" class="async-image hide" src="<?php echo $texonomy_image_url; ?>" data-src="<?php echo $texonomy_image_url ?>" alt="" />
        <?php } else {
			$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
			if($thumbnail[0]){
        ?>
        	<img class="async-image hide" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
        <?php } ?>
    
    </div>
    
    <?php } ?>
        <div class="title hidden" id="title">
            <img src="http://localhost/projects2/orvi/wp-content/themes/orvi/img/white-logo.png" alt="Orvi" width="120">
            <?php orvi_page_title(); ?>
        </div>

</header>

	<?php } else { ?>

    <header class="header">
    
        <div class="bg-img">
        <?php
			if(is_archive()){
			if (function_exists('z_taxonomy_image_url')) $texonomy_image_url = z_taxonomy_image_url(); ?>
			<img style="display:none;" class="async-image hide" src="<?php echo $texonomy_image_url; ?>" data-src="<?php echo $texonomy_image_url ?>" alt="" />
			<?php
				} elseif(is_front_page()) {
				$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
				if($thumbnail[0]){
			?>
        	<img class="async-image hide" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
        <?php  } } else {
				$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
				if($thumbnail[0]){
			?>
        	<img class="async-image hide" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
        <?php } } ?>
        
        <?php
        # section for X and V range
        if(is_front_page()){ ?>
            <div class="mm-x-range-section">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/x.png" />
                <p><?php echo get_option('wpc_xrange'); ?></p>
                <h2><a href="<?php echo home_url('?_set_range_parameter=1&range=x'); ?>">Discover The X Range</a></h2>
                
            </div>
        
            <div class="mm-y-range-section">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v.png" />
                <p><?php echo get_option('wpc_vrange'); ?></p>
                <h2><a href="<?php echo home_url('?_set_range_parameter=1&range=v'); ?>">Discover The V Range</a></h2>
                
            </div>
        <?php } ?>
        
        </div>
    <div class="title hidden" id="title">
    <?php if(is_single() || is_page()){ ?>
    	<a href="<?php echo home_url(); ?>">
            <img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120">
        </a>
    	<?php orvi_page_title(); ?>
    <?php } ?>
    </div>
    <div class="title hidden" id="title">
		<?php if(is_front_page()){ ?>
            <a href="<?php echo home_url(); ?>">
            	<img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120">
            </a>
        <?php } ?>
    </div>
    </header>
<?php } ?>

	
    

    <?php
	if(!is_front_page()){
		if(is_search()){ ?>
		<button class="trigger scroll-down-pulse" style="display:none;"><span><?php _e('Trigger','orvi'); ?></span></button>
		<?php } else { ?>
		<button class="trigger scroll-down-pulse"><span><?php _e('Trigger','orvi'); ?></span></button>
		<?php }
	} ?>
  
  
<script type="text/javascript">
	jQuery(function(){
		var var1 = setInterval(function(){
			jQuery(".fullwidth.sidebar-navigation-menu li ul").before("<div class='mobile_icon'><span></span></div>");
				
			clearInterval(var1);	
		}, 1500);
		
		jQuery(document).on('click', ".fullwidth.sidebar-navigation-menu li .mobile_icon", function(){
			jQuery(".fullwidth.sidebar-navigation-menu li ul").slideToggle();
		});
		
	});
</script>