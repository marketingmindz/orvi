<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */

get_header(); ?>

			<?php /*?><article class="content no-padding">
				<div class="title hidden" id="title2">
					<h1></h1>
					<p class="subline"></p>
				</div>
				<div class="no-margin">
					<div class="grid">
						<figure class="single-item-effect big">
							<img src="http://placehold.it/480x320/7f8c8d/ffffff" alt="img01"/>
							<figcaption>
								<div class="figcaption-border">
									<h2>Bad day in <span>Paris</span></h2>
									<p>Still better than a good day anywhere else</p>
									<a href="#">View more</a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
						<figure class="single-item-effect big">
							<img src="http://placehold.it/480x320/7f8c8d/ffffff" alt="img01"/>
							<figcaption>
								<div class="figcaption-border">
									<h2>Never too <span>old</span></h2>
									<p>Let’s go fall in love</p>
									<a href="#">View more</a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
						<figure class="single-item-effect full">
							<img src="http://placehold.it/965x350/7f8c8d/ffffff" alt="img01"/>
							<figcaption>
								<div class="figcaption-border">
									<h2>Bon <span>appetite</span></h2>
									<p>It is food for your soul</p>
									<a href="#">View more</a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
						<figure class="single-item-effect big">
							<img src="http://placehold.it/480x320/7f8c8d/ffffff" alt="img01"/>
							<figcaption>
								<div class="figcaption-border">
									<h2>I had it <span>all</span></h2>
									<p>But I was waiting for you</p>
									<a href="#">View more</a>
									<div class="figure-overlay"></div>	
								</div>
							</figcaption>											
						</figure>
						<figure class="single-item-effect big">
							<img src="http://placehold.it/480x320/7f8c8d/ffffff" alt="img01"/>
							<figcaption>
								<div class="figcaption-border">
									<h2>Like the <span>sunrise</span></h2>
									<p>Some things just have to happen</p>
									<a href="#">View more</a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
						<figure class="single-item-effect full">
							<img src="http://placehold.it/965x350/7f8c8d/ffffff" alt="img01"/>
							<figcaption>
								<div class="figcaption-border">
									<h2>Live your own <span>story</span></h2>
									<p>Don’t re-write someone elses</p>
									<a href="#">View more</a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
					</div>
				</div>
			</article><?php */?>

<?php get_footer(); ?>

