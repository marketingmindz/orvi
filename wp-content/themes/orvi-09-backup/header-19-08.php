<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles.css" /><!-- CSS: Main CSS -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize.css" /><!-- CSS: Normalize -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" /><!-- CSS: Font Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css" />
		<?php 
		if(is_page('gallery')){
		?>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/gallery.css" />
        <?php 
		}
		?>

	<?php wp_head(); ?>
    
</head>
<?php if($_SESSION['_range'] == "x" && is_page('orvicat')){ ?>
<style>
	.bg-img {
		background-color:#000;
	}
	body{
		background-color:#000;
	}
	
	.index-category .intro-effect-sidefixed.modify .title h1 {
		color:#fff;
	}
	
	.intro-effect-sidefixed .bg-img::before,
	.intro-effect-sidefixed .bg-img::after {
		content: '';
		position: absolute;
		z-index: 100;
	}
	.intro-effect-sidefixed .bg-img {
		background: #000 !important;
	}
	
	/*.intro-effect-sidefixed .bg-img::after {
	background: #f5f5f5  ;
	top: 0;
	right: 0;
	width: 60%;
	height: 100%;
	-webkit-transform: translateX(100%);
	transform: translateX(100%);
	z-index: 100;
	}*/
	
	.intro-effect-sidefixed.modify .bg-img::after {
		-webkit-transform: translateX(0);
		transform: translateX(0);
		background-color: black;
	}

</style>
<?php } elseif($_SESSION['_range'] == "v" && is_page('orvicat')) { ?>
<style>
	.bg-img {
		background-color:#fff;
	}
	body{
		background-color:#fff;
	}
	
	.index-category .intro-effect-sidefixed.modify .title h1 {
		color:#000;
	}
	
	
	.intro-effect-sidefixed.modify .bg-img::after {
		-webkit-transform: translateX(0);
		transform: translateX(0);
		background-color: black;
	}

</style>
<?php } ?>

<body <?php body_class(); ?>>
<div id="container" class="container <?php container_class(); ?>">
	<?php if(is_page('home')){ ?>
        <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0" style="display:none;">
            <span class="cd-menu-icon"></span>
        </a>
        <div id="travelogue-search" class="travelogue-search" style="display:none;">     
        	<?php get_search_form(); ?>
        </div>
    <?php } else { ?>
        <a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
            <span class="cd-menu-icon"></span>
        </a>
    
    	<div id="travelogue-search" class="travelogue-search">     
    		<?php get_search_form(); ?>
    	</div>
    <?php } ?>   
    <!-- EOF: Fancy Search Form (content imported via jQuery: from search-form.html) -->
    <?php if(is_search()){ ?>
    
    
		<header class="header" style="display:none;">
        <?php
        if(is_page("gallery", $classes)){
			$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
			if($thumbnail[0]){
			?>
			<img class="async-image hide" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
            <?php } ?>
		<?php }else{ ?>
        
        <div class="bg-img">
       	<?php
		if(is_archive()){
			if (function_exists('z_taxonomy_image_url')) $texonomy_image_url = z_taxonomy_image_url(); ?>
			<img class="async-image hide catimg" src="<?php echo $texonomy_image_url; ?>" data-src="<?php echo $texonomy_image_url ?>" alt="" />
			<?php
		} else {
			$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
			if($thumbnail[0]){
			?>
				<img class="async-image hide catimg" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
			<?php }
		} ?>
        
        </div>
        <?php } ?>
        <div class="title hidden" id="title">
			
			<img src="http://localhost/projects2/orvi/wp-content/themes/orvi/img/white-logo.png" alt="Orvi" width="120">
		
            <?php orvi_page_title(); ?>
        </div>
        
    </header>
	<?php } else { ?>
		<header class="header">
			<?php
			if(is_page("gallery", $classes)){
				$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
				if($thumbnail[0]){
				?>
				<img class="async-image hide" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
				<?php } ?>
			<?php }else{ ?>
			
			<div class="bg-img">
				<?php
				if(is_archive()){
					if (function_exists('z_taxonomy_image_url')) $texonomy_image_url = z_taxonomy_image_url(); ?>
					<img class="async-image hide catimg" src="<?php echo $texonomy_image_url; ?>" data-src="<?php echo $texonomy_image_url ?>" alt="" />
					<?php
				} else {
					$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' ); 
					if($thumbnail[0]){
					?>
					<img class="async-image hide catimg" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
					<?php }
					
					
				} ?>
				
				<?php
				# section for X and V range
				if(is_front_page()){ ?>
						<div class="mm-x-range-section">
							<a href="<?php echo home_url('?_set_range_parameter=1&range=x'); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/xwindow_thumb.jpg" />
							</a>
                            <p><?php echo get_option('wpc_xrange'); ?></p>
						</div>
						
						<div class="mm-y-range-section">
							<a href="<?php echo home_url('?_set_range_parameter=1&range=v'); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/vicon_thumb.jpg" />
							</a>
                            <p><?php echo get_option('wpc_vrange'); ?></p>

						</div>
				<?php } ?>
			
			</div>
			<?php } ?>
            
			<div class="title hidden" id="title">
				<?php if($_SESSION['_range'] == "x" && is_page('orvicat')){ ?>
                    <a href="<?php echo home_url(); ?>">
                    <img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120">
                    </a>
                    <h1>X RANGE</h1>
                
                <?php } elseif($_SESSION['_range'] == "v" && is_page('orvicat')) { ?>
                    <a href="<?php echo home_url(); ?>">
                    <img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="Orvi" width="120">
                    </a>
                    <h1>V RANGE</h1>
                <?php } else {?>
                	<a href="<?php echo home_url(); ?>">
                    <img src="<?php bloginfo('template_url'); ?>/img/white-logo.png" alt="Orvi" width="120">
                    </a>
                    <?php orvi_page_title(); ?>
                <?php } ?>
                
			</div>
			
		</header>
    <?php } ?>
    

    <?php
	if(!is_front_page()){
		if(is_search()){ ?>
		<button class="trigger scroll-down-pulse" style="display:none;"><span><?php _e('Trigger','orvi'); ?></span></button>
		<?php } else { ?>
		<button class="trigger scroll-down-pulse"><span><?php _e('Trigger','orvi'); ?></span></button>
		<?php }
	} ?>
  