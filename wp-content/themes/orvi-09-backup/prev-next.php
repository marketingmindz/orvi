<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
?>

<?php while ( have_posts() ) : the_post(); ?>
<div class="pagination grid">
<div class="pre-nex">
<?php
$prev_post = get_previous_post();
if(!empty($prev_post)): ?>
<figure class="single-item-effect half">
  
 <?php echo get_the_post_thumbnail($prev_post->ID, 'full'); ?>

    <figcaption>
        <div class="figcaption-border">
            <h2><i class="fa fa-angle-left"></i> <?php _e('Previous','orvi'); ?> <span><?php _e('Product','orvi'); ?></span></h2>
            <p class="product_title"><span><?php echo $prev_post->post_title; ?></span></p>
            <?php previous_post('%'); ?>
            <div class="figure-overlay"></div>
        </div>
    </figcaption>		
</figure>
<?php endif; ?>
<?php
$next_post = get_next_post();
if(!empty($next_post)): ?>
<figure class="single-item-effect half">
  <?php echo get_the_post_thumbnail($next_post->ID, 'thumbnail'); ?>
    <figcaption>
        <div class="figcaption-border">
            <h2><?php _e('Next','orvi'); ?> <span><?php _e('Product','orvi'); ?></span> <i class="fa fa-angle-right"></i> </h2>
            <p class="product_title"><span><?php echo $next_post->post_title; ?></span></p>
            <?php next_post('%'); ?>
            <div class="figure-overlay"></div>
        </div>
    </figcaption>								
</figure>
<?php endif; ?>
<div class="clearfix"></div>
</div>
</div>
<?php endwhile; ?>