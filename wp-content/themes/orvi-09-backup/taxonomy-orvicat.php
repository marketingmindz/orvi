
<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */

get_header(); 
 $CurrentTermsID = get_queried_object()->term_id;
 $categoryDesc = get_term_by('id', $CurrentTermsID, 'orvicat');
?>
<?php /*?><div class="relate_cats">
<h2>Other Categories</h2>


<?php 
	if($_SESSION['_range'] == "x") { ?>
<?php 
	$category = get_term_by('slug', 'x-range', 'orvicat' );
    $args = array(
	'show_option_all'    => '',
	'orderby'            => 'name',
	'order'              => 'ASC',
	'style'              => 'list',
	'child_of'           => $category->term_id,
	'exclude'            => '',
	'hierarchical'       => 1,
	'depth'              => 0,
	'title_li'		   => '',
	'taxonomy'           => 'orvicat',
	'walker'             => null
    );
    wp_list_categories( $args ); 
?>

<?php } ?>

<?php if($_SESSION['_range'] == "v"){ ?>
<?php 
	$category = get_term_by('slug', 'v-range', 'orvicat' );
    $args = array(
	'show_option_all'    => '',
	'orderby'            => 'name',
	'order'              => 'ASC',
	'style'              => 'list',
	'child_of'           => $category->term_id,
	'exclude'            => '',
	'hierarchical'       => 1,
	'depth'              => 0,
	'title_li'		   => '',
	'taxonomy'           => 'orvicat',
	'walker'             => null
    );
    wp_list_categories( $args ); 
?>
<?php } ?>

</div><?php */?>
<div class="relate_cats">
<?php if($_SESSION['_range'] == "v"){ ?>
<p><?php echo $categoryDesc->description; ?></p>
<?php } ?>
<?php if($_SESSION['_range'] == "x"){ ?>
<p><?php echo $categoryDesc->description; ?></p>
<?php } ?>
</div>
			<article class="content no-padding">
				
				<div class="no-margin">
					<div class="grid">
					
					<?php $a = 1; ?>
                    
						<?php
							 $list = array();

							$queried_object = get_queried_object();
							$term_taxonomy_id = $queried_object->term_taxonomy_id;
							$taxonomy = $queried_object->taxonomy;
							
							$catid = get_country();
							
							$args= array(
								'post_type' => 'orvi_products', 
								'post_status' => 'publish',
								'posts_per_page' => -1,
								'tax_query' => array(
									'relation' => 'AND',
									array(
										'taxonomy' => $taxonomy,
										'field' => 'term_id',
										'terms' => $term_taxonomy_id
									),
									array(
										'taxonomy' => 'countries',
										'field' => 'term_id',
										'terms' => $catid,
									)
								)
							);
							query_posts($args); 
							$the_query = new WP_Query( $args );

							if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						
                        <?php 
							if( $a % 3 == 0 ){
								$class_names = 'full';
							}else{
								$class_names = 'big';
							}
						?>
                        
                        <figure class="single-item-effect <?php echo $class_names; ?>">
                            <?php
								if ( has_post_thumbnail()):
									the_post_thumbnail('featured-image');
								endif;
							?>  
                              
                            <figcaption>
                                <div class="figcaption-border">
                                    <h2><?php the_title(); ?></h2>
                                    <a href="<?php the_permalink(); ?>"><?php _e('View more','orvi'); ?></a>
                                    <div class="figure-overlay"></div>
                                </div>
                            </figcaption>												
                        </figure>
                        <?php $a++; endwhile; ?>
                        <?php endif;  ?> 
                        
                      
					</div>
				</div>
			</article>

<?php get_footer(); ?>

