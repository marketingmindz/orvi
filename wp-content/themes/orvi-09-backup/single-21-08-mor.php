<?php
ob_start();
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
session_start();
global $post;
get_header();
$post_int_id = "";
?>

<script src="<?php bloginfo('template_url'); ?>/js/csspopup.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery("button#tog").click(function(){
        jQuery("#postbox").show("slow");
    });
});
</script>
<script>
function vali()
{
	var x = document.forms["new_post"]["title"].value;
	if ( x == null || x == ""){
		alert ("Please Enter a name");
		return false;
	}
	
	var x = document.forms["new_post"]["email"].value;
	var atpos=x.indexOf("@");
	var dotpos=x.lastIndexOf(".");
	if (x == null || x == ""){
	alert ("Enter an email");
	return false;
	}
	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	{
		alert("Not a valid email");
		return false;
	}
	var x=document.forms["new_post"]["phone"].value;
	if(x==null || x=="")
	{
		alert("Phone No. is required");
		return false;
	}
	else if(!new_post.phone.value.match(/^[0-9]+$/) && new_post.phone.value !="")
	{
		new_post.phone.value="";
		new_post.phone.focus(); 
		alert("only numbers");
		return false;
   }
	var x = document.forms["new_post"]["address"].value;
	if (x == null || x == "")
	{
		alert("Enter Address");
		return false;
	}
	
	
}
</script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/colorbox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox.js"></script>
<?php if(isset($_REQUEST['msg']) && $_REQUEST['msg']=="success"){ ?>
	<script>
	$(document).ready(function(){
		//Examples of how to assign the Colorbox event to elements
		$(".group1").colorbox({open:true, rel:'group1'});
		
	});
	</script>
<?php } ?>
<article class="content">

    <div>
    <?php //echo do_shortcode('[easy_image_gallery]'); ?>
    	<?php while ( have_posts() ) : the_post(); ?>
            <?php $post_int_id = get_the_ID();?>
        	<?php the_content(); ?>
         <?php endwhile; ?>
	</div>
</article>
	<?php /* Express Interest */ ?>
    <article class="content sendenq">
      
        <div id="blanket" style="display:none;"></div>
        <div id="popUpDiv" style="display:none;">
        	<a onclick="popup('popUpDiv')" class="closeenq" title="close"><?php _e('X','orvi'); ?></a>
            <div class="content-express">
            	<h2 class="head_sendenq"><?php _e('Send An Enquiry','orvi'); ?></h2>
                <p style="color:#666666; font:12px/19px 'Century Gothic';">
                    <?php _e('Your interest in the following product(s) has been noted. 
                    Please give us your details by clicking <strong>Send Request</strong> and we will be in touch.','orvi'); ?> 
                </p>
                <div id="ajax_msg"></div>
                <div class="entry_post">
                    <?php 
                          //  $array_session = $_SESSION['arr'];?>
                            <table style="width:100%" class="exp_enquiry">		
								
								
							</table>
                </div>
                
                <div class="two_buttons">
                    <div class="enq-buttons_cont"><a onclick="popup('popUpDiv')"><?php _e('Continue Browsing','orvi'); ?></a></div>
                    <button id="tog" class="enq-buttons"><?php _e('Send Enquiry','orvi'); ?></button>
                    <div class="clear"></div>
                </div>
				<?php 
				
				
				
				if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_post") {

    // Do some minor form validation to make sure there is content
    if (isset ($_POST['title'])) {
        $title =  $_POST['title'];
    } else {
        echo 'Please enter a  name';
    }
	if(isset($_POST['email'])){
		$senderemail = $_POST['email'];
	}else{
		
	}
	if(isset($_POST['phone'])){
		$senderphone = $_POST['phone'];
	}else{
		
	}
    if (isset ($_POST['address'])) {
        $description = $_POST['address'];
    } else {
        echo 'Please enter the content';
    }
	//echo "ravii".$_POST['proidsfromajax'];
	//die;
    $addedProid =  $_POST['proidsfromajax'];
	
	
	//Mail for product Enquiry
	include('page-templates/email.php');
	
    // Add the content of the form to $post as an array
    $new_post = array(
        'post_title'    => $title,
        'post_content'  => $description,
        //'post_category' => array($_POST['cat']),  // Usable for custom taxonomies too
        'tags_input'    => array($tags),
        'post_status'   => 'publish',           // Choose: publish, preview, future, draft, etc.
        'post_type' => 'enquiries'  //'post',page' or use a custom post type if you want to
    );
	
    //save the new post
    $pid = wp_insert_post($new_post);
	add_post_meta($pid, 'name', $title);
	add_post_meta($pid, 'email', $senderemail);
	add_post_meta($pid, 'phone', $senderphone);
	add_post_meta($pid, 'address', $description);
	add_post_meta($pid, 'product_ids',$addedProid); 
	
    //insert taxonomies
	
	
	wp_redirect(get_the_permalink()."?msg=success");
	exit;
	
}
				?>
                <?php /* New post type */ ?>
                
                
                <div id="postbox" style="display:none;" class="postbox">
                
               
                <form id="new_post" name="new_post" method="post" action="" onsubmit="return vali();">
                
                
                <p><label for="title"><?php _e('Name :','orvi'); ?></label><br />
                <input type="text" id="title" value="" tabindex="1" size="20" name="title" />
                </p>
                
                <p><label for="post_email"><?php _e('Email :','orvi'); ?></label><br />
                <input type="text" value="" tabindex="5" size="20" name="email" id="email" /></p>
                
                <p><label for="post_phone"><?php _e('Phone :','orvi'); ?></label><br />
                <input type="text" value="" tabindex="5" size="20" name="phone" id="phone" /></p>
                
                <p><label for="address"><?php _e('Address :','orvi'); ?></label><br />
                <textarea id="address" name="address" rows="4"></textarea>
                </p>
                
                <div class="form_buttons">
                <input type="hidden" class="proidsfromajax" name="proidsfromajax"  />
                <input type="submit" value="Send Enquiry" tabindex="6" id="submit" name="submit" />
                
                <a class="group1" href="<?php bloginfo('template_url'); ?>/img/hh_thumb.png" style="display:none;" title=""></a>
                <div class="enq-buttons_cont"><a onclick="popup('popUpDiv')"><?php _e('Continue Browsing','orvi'); ?></a></div>
                <div class="clear"></div>
                <input type="hidden" name="action" value="new_post" />
                </div>
                
                <?php wp_nonce_field( 'new-post' ); ?>
                </form>
                </div>
                
                <?php /* New post type */ ?>
            </div>
        </div>
        <div><a onclick="add_product_contact('<?php echo $post_int_id;?>')" class="exp_int"><?php _e('Express Interest','orvi'); ?></a></div>
    	
    </article>
    <?php /* Express Interest */ ?>
    
    
    <article class="content relate no-padding">
    	<div class="no-margin">
            <div class="grid">
            	<?php 
				$related_post = get_post_meta(get_the_ID(), 'related_post_id', true );
				$show_posts = explode(",",$related_post);
				$show_posts = array_filter($show_posts);
				
				if(count($show_posts)>0){
				?>
            	<h2 style="text-align:center;"><?php _e('Related Products', 'orvi'); ?></h2>
                <?php 
					
					
						foreach($show_posts as $show_post){
						$rel_post = get_post($show_post);
						
					?>
					<figure class="single-item-effect small">
						<?php echo get_the_post_thumbnail( $show_post, 'large' ); ?>
						<figcaption>
							<div class="figcaption-border">
								<h2><?php echo $rel_post->post_title; ?></h2>
							   
								<a href="<?php echo get_permalink($show_post); ?>"><?php _e('View more', 'orvi'); ?></a>
								<div class="figure-overlay"></div>
							</div>
						</figcaption>												
					</figure>
					<?php }
					} ?>
                <div class="clear"></div>
            </div>
        </div>
    </article>
    	
		<?php get_template_part( 'prev-next', get_post_format() ); ?>
    <script>	 
	   function delete_product(prouctnuid){
		   //console.log(prouctid);
			jQuery.ajax({
					url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
					type: "POST",
  					data: { prouctnuid : prouctnuid,act:1},
					}).done(function(result) {
						proidss = result.split("--"); 
						jQuery("#popUpDiv").show();
						jQuery(".exp_enquiry").empty();
					    jQuery(".exp_enquiry").append(proidss[0]);
						jQuery('#ajax_msg').text();
						jQuery('#ajax_msg').text(proidss[1]);
					});
	  }
	 
	 function add_product_contact(prouctid){
       console.log(prouctid);
        jQuery.ajax({
                url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
                type: "POST",
                data: { prouctid : prouctid },
                }).done(function(result) {
					proidss = result.split("--"); 
					console.log(result);
                    jQuery("#popUpDiv").show();
					jQuery(".exp_enquiry").empty();
					jQuery(".exp_enquiry").append(proidss[0]);
					jQuery('.proidsfromajax').val(proidss[2]);
					jQuery('#ajax_msg').text();
					jQuery('#ajax_msg').text(proidss[1]);
					//alert(proidss[1]);
                });
    }
    
    </script>  
      
<?php get_footer(); ?>
