<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
 global $wpdb;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/styles.css" /><!-- CSS: Main CSS -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize.css" /><!-- CSS: Normalize -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" /><!-- CSS: Font Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css" />
		
	<?php wp_head(); ?>
    
</head>
<body class="index-contact">

		<div id="container" class="container">

		<a class="cd-primary-nav-trigger" id="trigger-menu" href="#0">
		  	<span class="cd-menu-icon"></span>
		</a>

		<!-- BOF: Fancy Search Form (content imported via jQuery: from search-form.html) -->
		<div id="travelogue-search" class="travelogue-search">
			<form action="">
                <input class="travelogue-search-input" placeholder="<?php _e('Enter your search term...', 'orvi'); ?>" type="text" value="" name="search" id="search">
                <input class="travelogue-search-submit" type="submit" value="">
                <span class="travelogue-icon-search fa fa-search"></span>
        	</form>
		</div>
		<!-- EOF: Fancy Search Form (content imported via jQuery: from search-form.html) -->

			<header class="header">
				<div class="bg-img">
                <?php
					$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full' ); 
			if($thumbnail[0]){
			?>
			<img class="async-image hide" src="<?php echo $thumbnail[0]; ?>" data-src="<?php echo $thumbnail[0] ?>" alt="" />
			<?php } ?>
					<div class="overlay hidden" id="overlay"></div>
				</div>
				<div class="title">
					<h1>
						<span><?php _e('CONTACT','orvi'); ?></span>
						<span><?php _e('US','orvi'); ?></span>
					</h1>
					<p class="subline"><?php _e("Don't be a stranger",'orvi'); ?></p>