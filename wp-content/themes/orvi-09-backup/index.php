<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

                <?php if(count($posts)==0){?>
                <h2 class="search_head"><?php _e('No result found','orvi'); ?></h2>
					
				<?php }else{
					?>
                	<h2 class="search_head">
                    <?php _e('Search Result :','orvi'); ?> <?php echo count($posts);?></h2>
                <?php }?>
            <article class="content search_coustom_in">
    	<div class="search_post_in">
    		<div class="grid">	
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
            
				<?php get_template_part( 'content', 'search'); ?>
                
			<?php endwhile; ?>
<?php endif; ?>
</div>
		</div>
	</article>
		

<?php get_footer(); ?>