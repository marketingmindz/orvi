<form action="<?php echo home_url('/'); ?>">
<input class="travelogue-search-input" placeholder="Enter your search term..." value="<?php echo get_search_query(); ?>" name="s" id="s">
<input class="travelogue-search-submit" type="submit" id="searchsubmit" value=""/>
<span class="travelogue-icon-search fa fa-search"></span>
</form>

<?php /*?>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
	</div>
</form><?php */?>