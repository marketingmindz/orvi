<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Orvi
 * @since Orvi 1.0
 */
 get_header();
?>
    
				<?php
					$z = 1;
						$x = 1;				
					$postid = get_the_ID();
					$get_product_categories = get_the_terms($post->ID, 'orvicat' );
						if(!empty($get_product_categories)){
						foreach($get_product_categories as $get_product_category){
						//print_r($get_product_category);					
						$product_nmae = $get_product_category->name;
						$product_cate_id = $get_product_category->term_id;
						$product_slug = $get_product_category->slug;
						$productparentcate =  $get_product_category->parent;
						
						if( $x%2 == 1 ){
							$class_home = 'small';
						}else{
							$class_home = 'big';
						}
						
						if($z%2==0) $x++;
                ?>             	
                    <figure class="single-item-effect <?php echo $class_home ?>" id="post-<?php the_ID(); ?>">
						<?php echo get_the_post_thumbnail( $post_id);  ?>
                            <figcaption>
                                <div class="figcaption-border">
                                    <h2><?php the_title(); ?></h2>
                                    	<?php //the_content();  ?>
                                    	<a href="<?php echo esc_url( get_permalink() ); ?>"><?php _e('View more'); ?></a>
                                    <div class="figure-overlay"></div>
                                </div>
                            </figcaption>												
                    </figure>
                    
                <?php $z++;
					}
					}
                ?>
			
<?php //get_footer(); ?>