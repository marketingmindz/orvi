<?php
/*
** Template name:Category Page
*/
get_header();
?>  
          
           
           	<?php if($_SESSION['_range'] == "x"){ ?>
			
            <article class="content no-padding color-x">
                <div class="no-margin">
                	<div class="grid">
						<?php
						$term_id = 46;
						$taxonomy_name = 'orvicat';
						$termchildren = get_term_children( $term_id, $taxonomy_name );
						?>
                        <?php 
                        foreach ( $termchildren as $child ) {
							
							if( $i % 3 == 0 ){
								$class_name = 'full';
							}else{
								$class_name = 'big';
							}
							$term = get_term_by( 'id', $child, $taxonomy_name );
							
						?>
                        
                        <figure class="single-item-effect <?php echo $class_name ?>">
                        	<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
							<img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                       
							<figcaption>
								<div class="figcaption-border">
                                
									<h2><?php echo $term->name; ?></h2>
									<a href="<?php echo get_term_link( $child, $taxonomy_name ); ?>"><?php _e('View more','orvi'); ?></a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
							
						
                        <?php $i++; } ?> 
                    </div>
                </div>
            </article>
            <?php } ?>
            
            <?php if($_SESSION['_range'] == "v"){ ?>
			
            <article class="content no-padding color-v">
                <div class="no-margin">
                	<div class="grid">
					<?php
					$term_id = 47;
					$taxonomy_name = 'orvicat';
					$termchildren = get_term_children( $term_id, $taxonomy_name );
					?>
                     <?php 
                        foreach ( $termchildren as $child ) {
							
							if( $i % 3 == 0 ){
								$class_name = 'full';
							}else{
								$class_name = 'big';
							}
							$term = get_term_by( 'id', $child, $taxonomy_name );
							
						?>
                        
                        <figure class="single-item-effect <?php echo $class_name ?>">
                        	<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
							<img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                       
							<figcaption>
								<div class="figcaption-border">
                                
									<h2><?php echo $term->name; ?></h2>
									<a href="<?php echo get_term_link( $child, $taxonomy_name ); ?>"><?php _e('View more','orvi'); ?></a>
									<div class="figure-overlay"></div>
								</div>
							</figcaption>												
						</figure>
							
						
                        <?php $i++; } ?>    
                    </div>
                </div>
            </article>
            <?php } ?>
<?php get_footer(); ?>