<?php
/*
** Template name:Contact Us Page
*/
ob_start();
get_header();

?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/colorbox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox.js"></script>
<?php if(isset($_REQUEST['msg']) && $_REQUEST['msg']=="success"){ ?>
	<script>
	$(document).ready(function(){
		//Examples of how to assign the Colorbox event to elements
		$(".group1").colorbox({open:true, rel:'group1'});
		
	});
	</script>
<?php } ?>
<script>
function vali()
{
	var x = document.forms["myform"]["cont_name"].value;
	if ( x == null || x == ""){
		alert ("Please Enter a name");
		return false;
	}
	
	var x = document.forms["myform"]["cont_email"].value;
	var atpos=x.indexOf("@");
	var dotpos=x.lastIndexOf(".");
	if (x == null || x == ""){
	alert ("Enter an email");
	return false;
	}
	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	{
		alert("Not a valid email");
		return false;
	}
	
	var x = document.forms["myform"]["cont_about"].value;
	if (x == null || x == "")
	{
		alert("Please Enter What do you want to talk about?");
		return false;
	}
	
	var x = document.forms["myform"]["cont_detail"].value;
	if( x == null || x == "")
	{
		alert("Please give us more details");
		return false;
	}
	
	var x = document.forms["myform"]["captchacode"].value;
	if( x == null || x == "")
	{
		alert('Please enter the captcha value');
		return false;
	}
	
	
}
</script>


<?php

if(isset($_POST['submit_contact']))

{ 
			if(empty($_SESSION['6_letters_code'] ) ||
				strcasecmp($_SESSION['6_letters_code'], $_POST['6_letters_code']) != 0)
				{
				//Note: the captcha code is compared case insensitively.
				//if you want case sensitive match, update the check above to
				// strcmp()
				//echo "<div style='color:red;'>\n The captcha code does not match!</div>";
				echo "<script>
						alert('The captcha code does not match!');
						</script>";
			}
			else{
	$mail = get_option('admin_email');
	$subject = 'Contact';
	$q1 = $_POST['cont_name'];
	$q2 = $_POST['cont_email'];
	$q3 = $_POST['cont_about'];
	$q4 = $_POST['cont_detail'];
	$header  = 'MIME-Version: 1.0' . "\r\n";
	$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$send_message = "Name : $q1"."<br>";
	$send_message .= "Email : $q2"."<br>";
	$send_message .= "About : $q3"."<br>";
	$send_message .= "Details : $q4"."<br>";
	wp_mail($mail, $subject, $send_message, $header);
	
	wp_redirect(get_the_permalink()."?msg=success");
	exit;
}
}
	
?>



					
				</div>
                
			</header>
            
            <div class="contact-form">
						<?php /*?><form id="theForm" class="simform" autocomplete="off" method="POST" action="" name="form1">
							<div class="simform-inner">
								<ol class="questions">
									<li>
										<span><label for="q1"><?php _e("What's your name?",'orvi'); ?></label></span>
										<input id="q1" class="focus-me" name="q1" type="text"/>
									</li>
									<li>
										<span><label for="q2"><?php _e("What's your email address?",'orvi'); ?></label></span>
										<input id="q2" class="focus-me" name="q2" type="email"/>
									</li>
									<li>
										<span><label for="q3"><?php _e('What do you want to talk about?','orvi'); ?></label></span>
										<input id="q3" class="focus-me" name="q3" type="text"/>
									</li>
									<li>
										<span><label for="q4"><?php _e('Can you give me more details?','orvi'); ?></label></span>
										<textarea id="q4" name="q4" class="focus-me"></textarea>
									</li>
                                    
                                    <li>
										<span><label for="q5"><?php _e('Captcha','orvi'); ?></label></span>
                                        <img src="<?php echo get_template_directory_uri(); ?>/captcha/captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg'><br>
										<input type="text" id="q5" name="q5" required value="<?php echo isset($_POST['captchacode']) ? $captchacode : ''; ?> " ><br>
									</li>
                                    
								</ol><!-- /questions -->
                                
								<button class="submit" name="submit_contact" type="submit"><?php _e('Send answers','orvi'); ?></button>
                                
								<div class="controls">
									<button class="next"></button>
									<div class="progress"></div>
									<span class="number">
										<span class="number-current"></span>
										<span class="number-total"></span>
									</span>
									<span class="error-message"></span>
								</div><!-- / controls -->
							</div><!-- /simform-inner -->
							<span class="final-message"></span>
						</form><!-- /simform --><?php */?>
                        
                        
                        <form name="myform" method="post" autocomplete="off" class="simform" onsubmit="return vali();">
                        <div class="simform-inner">
                        	<div class="questions">
                            	<label><?php _e("What's your name?",'orvi'); ?></label>
                                <input type="text" name="cont_name" id="cont_name" class="focus-me" />
                            </div>
                            <div class="questions">
                            	<label><?php _e("What's your email address?",'orvi'); ?></label>
                                <input type="text" name="cont_email" id="cont_email" class="focus-me" />
                            </div>
                            <div class="questions">
                            	<label><?php _e("What do you want to talk about?",'orvi'); ?></label>
                                <input type="text" name="cont_about" id="cont_about" class="focus-me" />
                            </div>
                            <div class="questions">
                            	<label><?php _e("Can you give us more details?",'orvi'); ?></label>
                                <textarea id="cont_detail" name="cont_detail" class="focus-me" rows="5"></textarea>
                            </div>
                            <div class="questions">
                            	<label for="captcha"><?php _e('Captcha (*)'); ?></label><br />
                                <img src="<?php echo get_template_directory_uri(); ?>/captcha/captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg'><br>							   <input type="text" id="6_letters_code" name="6_letters_code" value="" class="focus-me" ><br>
                                <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
                            </div>
                            <input type="submit" class="submit-button" name="submit_contact" value="Send"/>
                            <a class="group1" href="<?php bloginfo('template_url'); ?>/img/contact-thanks.png" style="display:none;" title=""></a>
                            
                        </div>
                        </form>
                        

					</div>
	 
	<script language='JavaScript' type='text/javascript'>
		function refreshCaptcha()
		{
			var img = document.images['captchaimg'];
			img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
		}
    </script>

<?php get_footer(); ?>