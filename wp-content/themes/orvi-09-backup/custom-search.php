<?php
/**
* Template Name: Custom Search	
*/
get_header();

?>
<article class="content" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="">
        <div class="grid">
<?php 

echo $search_value = $_GET['search'];

$taxonomies = array('orvicat');
			
$args = array(
	'orderby'           => 'name', 
	'order'             => 'ASC',
	'hide_empty'        => true, 
	'exclude'           => array(), 
	'exclude_tree'      => array(), 
	'include'           => array(),
	'number'            => '', 
	'fields'            => 'all', 
	'slug'              => '',
	'parent'            => '',
	'hierarchical'      => true, 
	'child_of'          => 0,
	'childless'         => false,
	'get'               => '', 
	'name__like'        => $search_value,
	'description__like' => '',
	'pad_counts'        => false, 
	'offset'            => '', 
	'search'            => '', 
	'cache_domain'      => 'core'
); 

$terms = get_terms($taxonomies, $args);

echo "<pre>";
print_r($terms);
echo "</pre>";

$orvicat=array();
foreach($terms as $term){
	$orvicat[]=$term->term_id;
}
$z = 1;
						$x = 1;
						if( $x%2 == 1 ){
							$class_home = 'small';
						}else{
							$class_home = 'big';
						}
						
						if($z%2==0) $x++;			
$args = array(
	's' => $search_value,
	'post_type' => 'orvi_products',
	'meta_key'		=> 'sizes',
	'meta_value'	=> $search_value,
	'tax_query' => array(
		array(
			'taxonomy' => 'orvicat',
			'field'    => 'term_id',
			'terms'    => $orvicat,
			'operator' => 'IN',
		)
	),
	'relation' => 'OR'
	
);

$query = new WP_Query( $args );
echo "<pre>";
print_r($query);
echo "</pre>";

?>

    <h3><?php _e('Search Result for :','orvi'); ?> <?php echo "$s"; ?> </h3>  
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>   
        <figure class="single-item-effect <?php echo $class_home ?>">
			<?php if (function_exists('z_taxonomy_image_url')) $taxonomy_image_url = z_taxonomy_image_url($term->term_id); ?>
                <img src="<?php echo $taxonomy_image_url; ?>" alt="img01"/>
                <figcaption>
                    <div class="figcaption-border">
                        <h2><?php the_title(); ?></h2>
                        	<a href="<?php echo esc_attr(get_term_link($term, "orvicat")); ?>"><?php _e('View more','orvi'); ?></a>
                        <div class="figure-overlay"></div>
                    </div>
            	</figcaption>												
        </figure>
        <?php $z++; endwhile; endif; ?>
        </div>
    </div>
</article><!-- #post-## -->

<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////?>

<?php ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////?>


<?php get_footer(); ?>

