<?php 
/**
* Template name:Products Demo Page
**/
get_header();
?>
<div class="boxs">
        <?php 
				
				//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				
				

				$args = array( 'post_type' => 'orvi_products', 'posts_per_page' =>-1,'order'=>'ASC');
				$wpex_query = new WP_Query( $args );
				while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
				//'paged' => $paged,
				?>
                <div class="exclusive_box">
					<?php
                    if ( has_post_thumbnail()):
                        the_post_thumbnail('featured-image');
                    endif;
                    ?>                
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    
				</div>
            <?php endwhile;wp_reset_postdata(); ?>     

  <?php  ?>
            <div class="clear"> </div>
            
	</div>

<?php get_footer(); ?>