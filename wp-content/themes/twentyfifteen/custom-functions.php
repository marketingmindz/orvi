<?php

/*** Products Post-type ***/

add_action('init', 'create_orviproducts');

function create_orviproducts() {

    $labels = array(
        'name' => __('Orvi Products', 'post type general name','orvi'),
        'singular_name' => __('Orvi Products', 'post type singular name','orvi'),
        'add_new' => __('Add Orvi Products','orvi'),
        'add_new_item' => __('Add Orvi Products','orvi'),
        'edit_item' => __('Edit Orvi Products','orvi'),
        'new_item' => __('New Orvi Products','orvi'),
        'view_item' => __('View Orvi Products','orvi'),
        'search_items' => __('Search Orvi Products','orvi'),
        'not_found' =>  __('Nothing found','orvi'),
        'not_found_in_trash' => __('Nothing found in Trash','orvi'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
      ); 

    register_post_type( 'orvi_products' , $args );
}
////// Range Taxanomy/////////
add_action( 'init', 'create_range_tax' );

function create_range_tax() {
	register_taxonomy(
		'orvi_category',
		'orvi_products',
	array(
		'label' => __( 'Orvi Category', 'orvi' ),
		'rewrite' => array( 'slug' => 'orvi_category' ),
		'hierarchical' => true,
	)
	);
}

/*** End Products Post-type ***/
?>


<?php

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function orvitheme_add_meta_box() {

	$screens = array( 'orvi_products' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'orvitheme_sectionid',
			__( 'Related Posts', 'orvitheme_textdomain' ),
			'orvitheme_add_meta_box_callback',
			$screen
		);
	}
}
add_action( 'add_meta_boxes', 'orvitheme_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function orvitheme_add_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'orvitheme_meta_box', 'orvitheme_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, 'related_post_id', true );
	$myrelated_postid_allexp = explode(",", $value);
?>
	<div class="ddsd">
	<?php
		
		$args = array( 'post_type' => 'orvi_products', 'posts_per_page' =>-1,'order'=>'ASC');
		$i = 0;
			$wpex_query = new WP_Query( $args );
			
			while ( $wpex_query->have_posts() ) : $wpex_query->the_post(); 
			
			?>
            <div>
   				<p>
                	<input type="checkbox" value="<?php echo get_the_ID(); ?>" <?php if(in_array(get_the_ID(), $myrelated_postid_allexp)){ ?> checked="checked" <?php } ?> id="myrelated_postid" name="myrelated_postid[]" /><?php the_title(); ?>
                </p>
                
            </div>
            
    <?php
	
  		endwhile; wp_reset_query();
		
		?>
        </div>
<?php
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function orvitheme_save_meta_box_data( $post_id ) {

$post_type = $_POST['post_type'];

	if($post_type == 'orvi_products' ){
	/*echo '<pre>';
	print_r($_REQUEST['myrelated_postid']);
	echo '</pre>';*/
	$myrelated_postid_all = implode(",", $_REQUEST['myrelated_postid']);
	//exit;

	if(count($_REQUEST['myrelated_postid']> 0)){
		
		update_post_meta( $post_id, 'related_post_id', $myrelated_postid_all );
		
		}

	
		// Update the meta field in the database.
		
		
	}
}
add_action( 'save_post', 'orvitheme_save_meta_box_data' );

?>
<?php
/**
 * Proper way to enqueue scripts and styles
 */
function orvitheme_scripts() {
	wp_enqueue_style( 'new-style', get_template_directory_uri().'/new-style.css' );
	//wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
}

add_action( 'admin_enqueue_scripts', 'orvitheme_scripts' );